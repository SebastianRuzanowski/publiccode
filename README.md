Repository of Sebastian Rużanowski,
Code shared only for the recruitment process.
C# projects:

###Conversation analyzer v1.0 ###
Conversation based on facebook conversations:
- scrolls through the page to the end of the conversation,
- basic information about a conversation,

###Conversation analyzer v2.0###
- More extended information about conversation:

+ Time needed to answer between two or more conversationalists,
+ Most commonly used words(3x short,3x medium,3x long),
+ Most frequently used forbidden words(insults),
+ Number of messages by single person or whole conversation,
+ Constituing percent of messages in conversation,
+ Average messages per day,
+ Message with maximal length from entire conversation,
+ Maximal number of messages per day or minute by person,
+ Days since first message to last,
+ Day of first message,
+ Days without responding,
+ Number of mistakes,
+ Number of declarations of love,
+ Number of thankgivings,

Many more in progress.



All rights reserved.