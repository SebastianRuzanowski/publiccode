﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Repository.CategoryRepo;
using Repository.Models;
using Repository.Models.ViewModels;

namespace Announcements.Controllers
{
    public class CategoriesController : Controller
    {
        //  private AdContext db = new AdContext();
        private readonly ICategoryRepo repo;
        // GET: Categories
        public CategoriesController(ICategoryRepo repo)
        {
            this.repo = repo;
        }

        public ActionResult Index()
        {
            var categories = repo.FetchCategories();

            return View(categories.ToList());
        }

        public ActionResult ShowAnnouncements(int id)
        {
            var announcements = repo.FetchAnnouncementsFromCategories(id).ToList();
            AnnouncementsFromCategoriesViewModels model = new AnnouncementsFromCategoriesViewModels();
            model.Announcements = announcements.ToList();
            model.CategoryName = repo.NameForCategory(id);
            return View(model);
        }
        [Route("JSON")]
        public ActionResult CategoriesInJSON()
        {
            var categories = repo.FetchCategories().AsNoTracking();
            return Json(categories, JsonRequestBehavior.AllowGet);
        }
    }
}
