﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Repository.IAnnouncementRepo;
using Repository.Methods;
using Repository.Models;
using Microsoft.AspNet.Identity;
using PagedList;
namespace Announcements.Controllers
{
    public class AnnouncementsController : Controller
    {
        private readonly IAnnouncementRepo repository;

        // GET: Announcements
        public ActionResult Index(int? page, string sortOrder)
        {

            //var announcements = repository.FetchAnnouncements();
            var currentPage = page ?? 1;
            var eachPage = 5;
            var announcements = repository.FetchAnnouncements();
         //  announcements = announcements.OrderByDescending(x => x.DateAdded);
            ViewBag.CurrentSort = sortOrder;
            ViewBag.idSort = String.IsNullOrEmpty(sortOrder) ? "IdAsc" : "";
            ViewBag.DateAddedSort = sortOrder == "DateAdded" ? "DateAddedAsc" : "DateAdded";
            ViewBag.ContentSort = sortOrder == "ContentAsc" ? "Content" : "ContentAsc";
            ViewBag.TitleSort = sortOrder == "TitleAsc" ? "Title" : "TitleAsc";

            switch (sortOrder)
            {
                case "DateAdded":
                    announcements = announcements.OrderByDescending(a => a.DateAdded);
                    break;
                case "DateAddedAsc":
                    announcements = announcements.OrderBy(a => a.DateAdded);
                    break;
                case "Title":
                    announcements = announcements.OrderByDescending(a => a.Title);
                    break;
                case "TitleAsc":
                    announcements = announcements.OrderBy(a => a.Title);
                    break;
                case "Content":
                    announcements = announcements.OrderByDescending(a => a.Content);
                    break;
                case "ContentAsc":
                    announcements = announcements.OrderBy(a => a.Content);
                    break;
                case "IdAsc":
                    announcements = announcements.OrderBy(a => a.Id);
                    break;
                default:
                    announcements = announcements.OrderByDescending(a => a.Id);
                    break;

            }

            return View(announcements.ToPagedList<Announcement>(currentPage,eachPage));
        }
        [OutputCache(Duration = 1000)]
        public ActionResult MyAnnouncements(int? page, string sortOrder)
        {

        
            var currentPage = page ?? 1;
            var eachPage = 5;
            var userId = User.Identity.GetUserId();
            var announcements = repository.FetchAnnouncements();
            announcements = announcements.OrderByDescending(x => x.DateAdded).Where(x => x.UserId == userId);
           
            return View(announcements.ToPagedList<Announcement>(currentPage, eachPage));
        }

        public AnnouncementsController(IAnnouncementRepo _announcementRepository)
        {
            repository = _announcementRepository;
        }

       
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Announcement announcement = repository.GetOgloszenieById((int)id);
            if (announcement == null)
            {
                return HttpNotFound();
            }
            return View(announcement);
        }

        [Authorize]
        public ActionResult Create()
        {

            return View();
        }

        // POST: Announcements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Content,Title")] Announcement announcement)
        {
            if (ModelState.IsValid)
            {
                announcement.UserId = User.Identity.GetUserId();
                announcement.DateAdded = DateTime.Now;
                try
                {
                    repository.AddAnnouncment(announcement);
                    repository.SaveChanges();
                    return RedirectToAction("MyAnnouncements");
                }
                catch (Exception)
                {
                    ViewBag.Error = true;
                    return View(announcement);

                }

            }
            return View(announcement);




        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Announcement announcement = repository.GetOgloszenieById((int)id);
            if (announcement == null)
            {
                return HttpNotFound();
            }
            else if (announcement.UserId != User.Identity.GetUserId() &&
                     !(User.IsInRole("Admin") || User.IsInRole("Employee")))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(announcement);
        }

        // POST: Announcements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Content,Title,DateAdded,UserId")] Announcement announcement)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    repository.Update(announcement);
                    repository.SaveChanges();
                }
                catch (Exception)
                {
                    ViewBag.Error = true;
                    throw;
                }
                ViewBag.Error = false;
                return RedirectToAction("Index");
            }

            return View(announcement);
        }

        [Authorize]
        public ActionResult Delete(int? id, int? error)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Announcement announcement = repository.GetOgloszenieById((int)id);
            if (announcement == null)
            {
                return HttpNotFound();
            }
            else if (announcement.UserId != User.Identity.GetUserId() &&
                 !(User.IsInRole("Admin")))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (error != null)
                ViewBag.Error = true;
            return View(announcement);
        }
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            repository.DeleteAnnouncement(id);
            try
            {
                repository.SaveChanges();
            }
            catch (Exception)
            {
                return RedirectToAction("Delete", new { id = id, error = true });
            }

            return RedirectToAction("Index");
        }


        [Authorize]
        public ActionResult Partial(int? page)
        {
            var currentPage = page ?? 1;
            var eachPage = 5;
            var announcements = repository.FetchAnnouncements();
            announcements = announcements.OrderByDescending(x => x.DateAdded);

            return PartialView("Index",announcements.ToPagedList<Announcement>(currentPage, eachPage));
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}

