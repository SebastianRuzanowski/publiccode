using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Repository.Models;

namespace Repository.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AdContext>
    {
       
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Repository.Models.AdContext context)
        {
            //if (System.Diagnostics.Debugger.IsAttached == false)
            //    System.Diagnostics.Debugger.Launch();
            //context = new AdContext();

            SeedRoles(context);
            SeedUsers(context);
            SeedAds(context);
            SeedCategories(context);
          SeedAnnoucement_Category(context);
          
        }

        private void SeedRoles(AdContext context)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists("Employee"))
            {
                var role = new IdentityRole();
                role.Name = "Employee";
                roleManager.Create(role);
            }
            context.SaveChanges();
        }

        private void SeedUsers(AdContext context)
        {
         
            var store = new UserStore<User>(context);
            var manager = new UserManager<User>(store);
            if (!context.Users.Any(x => x.UserName == "Mark"))
            {
                var user = new User {UserName = "Mark@AspNetMvc.com", Name = "Mark", Surname = "Spencer", Age = 30, DateOfBirth = DateTime.Now.Date, Email = "Mark@AspNetMvc.com" };
                var adminresult = manager.Create(user, "qwerty");
                if (adminresult.Succeeded)
                    manager.AddToRole(user.Id, "Employee");
            }
            if (!context.Users.Any(x => x.UserName == "CEO"))
            {
                var user = new User { UserName = "Ceo@AspNetMvc.com",Name = "Director", Surname = "Sam", Age = 55, DateOfBirth = DateTime.Now.Date,Email = "Ceo@AspNetMvc.com" };
                var adminresult = manager.Create(user, "qwerty");
                if (adminresult.Succeeded)
                    manager.AddToRole(user.Id, "Admin");
            }
            context.SaveChanges();
        }

        private void SeedAds(AdContext context)
        {
            var firstOrDefault = context.Set<User>().FirstOrDefault(x => x.UserName == "Mark");
            if (firstOrDefault != null)
            {
                var idUser = firstOrDefault.Id;
                for (int i = 1; i <= 10; i++)
                {
                    var ad = new Announcement()
                    {
                        Id = i,
                        UserId = idUser,
                        Content = "Ad Content " + i,
                        Title = "Ad Title " + i,
                        DateAdded = DateTime.Now.AddDays(-1)
                    };
                    context.Set<Announcement>().AddOrUpdate(ad);
                }
            }
            context.SaveChanges();
        }

        private void SeedCategories(AdContext context)
        {
            for (int i = 1; i <= 10; i++)
            {
                var category = new Category()
                {
                    Id = i,               
                    Name = "Category Name " + i,
                    Content = "Category content " + i,
                    MetaTitle = "Category Name " + i,
                    MetaKeys = "Category description " + i,
                    MetaDescription = "Meta Keys to category " + i,
                    ParentId = 1
                };
                context.Set<Category>().AddOrUpdate(category);
            }
            context.SaveChanges();
        }

        private void SeedAnnoucement_Category(AdContext context)
        {
            for (int i = 1; i < 10; i++)
            {
                var ad_c = new Announcement_Category()
                {
                    Id = i,
               //     AnnouncementId = 1,
                    CategoryId = i/2+1,
                    AnnouncementId = i/2+2
                };
                context.Set<Announcement_Category>().AddOrUpdate(ad_c);
            }
            context.SaveChanges();
        }
    }
    
}
