﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using Repository.Models;
using Microsoft.AspNet.Identity;
using Repository.IAnnouncementRepo;

namespace Repository.Methods
{
    public class AnnouncementRepo : IAnnouncementRepo.IAnnouncementRepo
    {
        private readonly AdContext db;

        public AnnouncementRepo(AdContext db)
        {
            this.db = new AdContext();
        }

        public IQueryable<Announcement> FetchAnnouncements()
        {
            db.Database.Log = message => Trace.WriteLine(message);
            return db.Announcements.AsNoTracking();
        }

        public Announcement GetOgloszenieById(int id)
        {
            Announcement ad = db.Announcements.Find(id);
            return ad;
        }

        public void DeleteAnnouncement(int id)
        {
            DeleteRelatedAnnouncementCategory(id);
               Announcement announcement = GetOgloszenieById(id);
            db.Announcements.Remove(announcement);
        }

        public void AddAnnouncment(Announcement announcement)
        {
            db.Announcements.Add(announcement);
        }

        private void DeleteRelatedAnnouncementCategory(int idAd)
        {
            var list = db.Announcement_Categories.Where(x => x.AnnouncementId == idAd);
            foreach (var announcementCategory in list)
            {
                db.Announcement_Categories.Remove(announcementCategory);
            }
        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }

        void IAnnouncementRepo.IAnnouncementRepo.Update(Announcement announcement)
        {
            db.Entry(announcement).State = EntityState.Modified;
        }

        public IQueryable<Announcement> FetchPage(int? page, int? pageSize)
        {
            var announcement =
                db.Announcements.OrderByDescending(x => x.DateAdded)
                    .Skip((page.Value - 1)*pageSize.Value)
                    .Take(pageSize.Value);
            return announcement;
        }
    }
}