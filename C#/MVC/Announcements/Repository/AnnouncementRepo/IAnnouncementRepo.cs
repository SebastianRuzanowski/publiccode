﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using Repository.Models;
namespace Repository.IAnnouncementRepo
{
    public interface IAnnouncementRepo
    {
        IQueryable<Announcement> FetchAnnouncements();
        Announcement GetOgloszenieById(int id);
        void DeleteAnnouncement(int id);
        void AddAnnouncment(Announcement announcement);
        void SaveChanges();
        void Update(Announcement announcement);
        IQueryable<Announcement> FetchPage(int? page, int? pageSize);

    }
}