﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Repository.Models;

namespace Repository.CategoryRepo
{
    public class CategoryRepo :ICategoryRepo
    {
        private readonly AdContext db;
        public IQueryable<Category> FetchCategories()
        {
            var categories = db.Categories.AsNoTracking();
            return categories;
        }

        public IQueryable<Announcement> FetchAnnouncementsFromCategories(int id)
        {
            var announcements = from o in db.Announcements
                join k in db.Announcement_Categories on o.Id equals k.Id
                where k.CategoryId == id
                select o;
            return announcements;
        }

        public CategoryRepo(AdContext repo)
        {
            db = repo;
        }
        public string NameForCategory(int id)
        {
            var name = db.Categories.Find(id).Name;
            return name;
        }
    }
}