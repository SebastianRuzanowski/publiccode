﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.Models;

namespace Repository.CategoryRepo
{
    public interface ICategoryRepo

    {
        IQueryable<Category> FetchCategories();
        IQueryable<Announcement> FetchAnnouncementsFromCategories(int id);
        string NameForCategory(int id);
    }
}
