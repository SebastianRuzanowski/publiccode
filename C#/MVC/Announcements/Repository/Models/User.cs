﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Repository.Models
{
    public class User : IdentityUser
    {
        public User()
        {
            this.Announcements = new HashSet<Announcement>();
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public int? Age { get; set; }
        
        [NotMapped]
        [Display(Name = "Sir or Madam: ")]
        public string FullName => Name + " " + Surname;
        public DateTime DateOfBirth { get; set; }
        public virtual ICollection<Announcement> Announcements { get; private set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}