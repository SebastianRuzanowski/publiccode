﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Repository.Models
{
    public class Announcement
    {
        public Announcement()
        {
            this.AnnouncementCategories = new HashSet<Announcement_Category>();
        }
       
        [Display(Name = "Id:")]
        public int Id { get; set; }

        [Display(Name = "Announcement's content")]
        [MaxLength(500)]
        public string Content { get; set; }
        [Display(Name = "Announcement's title")]
        [MaxLength(72)]
        public string Title { get; set; }
        [Display(Name = "Added: ")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateAdded { get; set; }
        
        public string UserId { get; set; }

        public virtual ICollection<Announcement_Category> AnnouncementCategories { get; set; }

        public virtual User User { get; set; }
    }
}