﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Repository.Models.ViewModels
{
    public interface IAnnouncementsFromCategoriesViewModels
    {
        IList<Announcement> Announcements { get; set; }
        string CategoryName { get; set; }
        
    }
    public class AnnouncementsFromCategoriesViewModels : IAnnouncementsFromCategoriesViewModels
    {
        private readonly AdContext db;
        public IList<Announcement> Announcements { get; set; }
        public string CategoryName { get; set; }

    }
}