﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Repository.Models
{
    public class Category
    {
        public Category()
        {
            this.AnnouncementCategories = new HashSet<Announcement_Category>();
        }
        [Key]
        [Display(Name="Category id: ")]
        public int Id { get; set; }

        [Required]
        [Display(Name ="Category Name: ")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Parent's id: ")]
        public int ParentId { get; set; }

        #region SEO
        [MaxLength(72)]
        [Display(Name = "Title on Google: ")]
        public string MetaTitle { get; set; }

        [MaxLength(160)]
        [Display(Name = "Description on Google: ")]
        public string MetaDescription { get; set; }

        [MaxLength(160)]
        [Display(Name = "Key words on Google: ")]
        public string MetaKeys { get; set; }

        [MaxLength(500)]
        [Display(Name = "Title on Google: ")]
        public string Content { get; set; }

        #endregion

        public ICollection<Announcement_Category> AnnouncementCategories { get; set; }
    }
}