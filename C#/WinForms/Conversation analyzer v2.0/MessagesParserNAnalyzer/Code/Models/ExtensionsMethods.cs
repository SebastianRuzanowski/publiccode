﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using MessagesParserNAnalyzer.Code.Models.EntityFramework;

namespace MessagesParserNAnalyzer
{
    
    public static class ExtensionMethods
    {
        private class UsedWords
        {
            static List<string> _usedWords = new List<string>();

            public static List<string> UsedWordsProperty
            {
                get { return _usedWords; }
                set { _usedWords.Add(value[0]); }
            } 
        }

        /// <summary>
        /// Overloaded method Contains, able to compare value with list of strings, 
        /// Keep in mind that "matchWords" must have ref key parameter.
        /// if value doesn't match with any matchWord, then that value is being
        /// added to matchWords pot.
        /// parameter usedWords is utilized to use usedWords list,
        /// true if needs to be used, otherwise false
        /// </summary>
        /// <param name="value" />
        /// <param name="matchWords" />
        /// <param name="usedWords"></param>
        /// <returns></returns>
        public static bool Contains(this string value, ref List<string> matchWords,bool usedWords=true)
        {
            foreach (var forbiddenWord in matchWords)
            {
                if (value.Contains(forbiddenWord))
                    return true;
            }
            if (usedWords)
            {
                foreach (var usedWord in UsedWords.UsedWordsProperty)
                {
                    if (value == usedWord)
                        return true;
                }
                UsedWords.UsedWordsProperty.Add(value);
            }
            return false;
        }
       

        public static void ClearUsedWords() =>
            UsedWords.UsedWordsProperty.Clear();

        public static void CreateDictionary(this Dictionary<DateTime, int> dictionary, IQueryable<FullView> query)
        {
            foreach (var q in query)
            {
                if (!dictionary.ContainsKey(q.dateTime.Date))
                    dictionary.Add(q.dateTime.Date, 1);
                else
                {
                    dictionary[q.dateTime.Date] += 1;
                }
            }
        }

        public static void SerializeDictionary<T, T1>(this Dictionary<T, T1> dictionary, string @path)
        {
            var splitPath = path.Split('\\');
            var path2 = "";
            for (int index = 0; index < splitPath.Length - 1; index++)
            {
                if (index == 0)
                {
                    path2 = path2 + splitPath[index];
                }
                else
                {
                    path2 = path2 + @"\" +
                           splitPath[index];
                }

            }
            Directory.CreateDirectory(@path2);
            XElement xElem = new XElement(
                        "items",
                        dictionary.Select(
                            x =>
                                new XElement("item", new XAttribute("Key", x.Key),
                                    new XAttribute("Value", x.Value)))
                    );
            var xml = xElem.ToString();
            
            using (var writer = new XmlTextWriter(@path, Encoding.UTF8))
            {
                xElem.WriteTo(writer);
            }
        }

        public static Dictionary<T, T1> DeserializeDictionary<T, T1>(this Dictionary<T, T1> dictionary, string @path)
            where T : IConvertible where T1 : IConvertible
        {


            XElement xElem2 = XElement.Load(new XmlTextReader(@path));

           var keys =  xElem2.Descendants("item").Select(x=>x.Attribute("Key").Value.ConvertType(typeof(T))).ToList();
            
            var values = xElem2.Descendants("item").Select(x => x.Attribute("Value").Value.ConvertType(typeof(T1))).ToList();
            

            for (int i = 0; i < keys.Count; i++)
            {
                dictionary.Add((T)keys[i],(T1)values[i]);   
            }

            return dictionary;

        }

        public static object ConvertType(this string input, Type t1)
        {
            TypeConverter typeConverter = TypeDescriptor.GetConverter(t1);
            return typeConverter.ConvertFromString(input);
        }


        public static T Sum<T>(this List<T> list) where T: IComparable
        {
            T result = default(T);
            dynamic result2 = result;
            foreach (var member in list)
            {

                result2 = member + result2;
            }
            return result2;
        }

    }
}
