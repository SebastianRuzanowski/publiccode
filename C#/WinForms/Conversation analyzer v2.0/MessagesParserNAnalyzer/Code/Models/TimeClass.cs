﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MessagesParserNAnalyzer.Code.Models.EntityFramework;

namespace MessagesParserNAnalyzer
{
    public class TimeClass
    {
        public List<float> dialogAvgCzasyOdp = new List<float>();
        public Dictionary<DateTime, string> startOfConversation = new Dictionary<DateTime, string>();
        public Dictionary<DateTime, int> _bigDelay = new Dictionary<DateTime, int>();
        public List<int> czasOdpowiedzi = new List<int>();
        
      


        /// <summary>
        /// To query select idOfConversation
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public TimeClass TimeNeededToAnswer(IQueryable<FullView> query, List<string> conversonalists)
        {
            dialogAvgCzasyOdp.Clear();
            startOfConversation.Clear();
            _bigDelay.Clear();
            czasOdpowiedzi.Clear();

            var query2 = query.ToList();
            var stopIndex = 0;

            for (int i = 0; i < query2.Count - 1; i++)
            {

                if (query2[i].personName == query2[i + 1].personName)
                {
                    if (stopIndex == 0)
                        stopIndex = i;

                    var difference = query2[i].dateTime.Subtract(query2[i + 1].dateTime).TotalMinutes;
                    if (difference >= 0)
                    {
                        try
                        {
                            if (difference > 300)
                            {
                                if (!startOfConversation.ContainsKey(query2[i + 1].dateTime))
                                    startOfConversation.Add(query2[i + 1].dateTime, query2[i + 1].personName);
                            }

                            else if (difference > 10 & difference < 300)
                            {
                                if (!_bigDelay.ContainsKey(query2[i + 1].dateTime))
                                    _bigDelay.Add(query2[i + 1].dateTime, (int) difference);
                            }


                        }
                        catch (Exception)
                        {
                            MessageBox.Show(query2[i + 1].dateTime + " " + difference);
                        }
                    }
                }
                else
                {
                    var difference = (int)query2[i].dateTime.Subtract(query2[i + 1].dateTime).TotalMinutes;
                    dialogAvgCzasyOdp.Add(difference);
                    if (difference>=0&&difference < 100)

                        czasOdpowiedzi.Add(difference);
                    else if(difference>0)
                    {
                        if(!startOfConversation.ContainsKey(query2[i+1].dateTime))
                        startOfConversation.Add(query2[i + 1].dateTime, query2[i + 1].personName);
                    }
                }
            }


            return this;
        }

        public int MostFrequentlyWrittenDayByPerson(Dictionary <DateTime, string> dictionary, string person)
        {
          dictionary =  dictionary.Where(x => x.Value == person).ToDictionary(x=>x.Key,x=>x.Value);

            return dictionary.Count;
        }
    }


}
