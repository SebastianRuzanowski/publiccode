﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessagesParserNAnalyzer.Code.Forms.AnalyseForm
{
    public static class Forbidden
    {
        public static List<string> forbiddenSymbols = new List<string>()
                {
                    ":",
                    "&",
                    "*",
                    "^",
                    "$",
                    "(",
                    ")",
                    "=",
                    "0",
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    ".",
                    ",",
                    "\"",
                    "\\",
                    "\'",
                    "/"
                };

        public static List<string> forbiddenWords = new List<string>()
        {
            "kurw",
            "huj",
            "slut",
            "cunt",
            "bitch",
            "fuck",
            "pierdol",
            "jeban",
            "śmieć",
            "smiec",
            "pizda",
            "pierdalaj",
            "gtfo",
            "ass",
            "shit",
            "twat"
        };
    }
}
