﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using MessagesParserNAnalyzer.Code.Models.EntityFramework;

namespace MessagesParserNAnalyzer
{
    public interface IAnswersCalculate
    {
        Dictionary<string, int> MostFrequentlyUsedWord(IQueryable<FullView> query);
        int TimeNeededToAnswerDynamic(IQueryable<FullView> query);
        int numberOfAllMessagesByUsers(int idOfConversation);
        int numberOfAllMessagesByUser(int idOfConversation, string nameConversonalist);
        double PercentageMessagesPerConversation(int idOfConversation, string nameConversonalist);
        string MostFrequentlyUsedInsult(int idOfConversation, string nameConversonalist, string param0,
            string param1, string param2, string param3, string param4, string param5, string param6);
        int MostFrequentlyUsedInsultNumber(int idOfConversation, string nameConversonalist, string param0,
            string param1, string param2, string param3, string param4, string param5, string param6);

         int DaysFromFirstDayToLast(Dictionary<DateTime, int> dictionary);
    }

    public class AnswersCalculate : IAnswersCalculate
    {


        public Dictionary<string, int> MostFrequentlyUsedWord(IQueryable<FullView> query)
        {
            var source = query.Select(x => x.content).ToList();

            var frequencies = new Dictionary<string, int>();
            var highestFreq = 0;
            var combinedString = string.Join(" ", source).Split(new[] { ' '});

            foreach (var word in combinedString)
            {

                int freq;
                frequencies.TryGetValue(word, out freq);
                freq += 1;

                if (freq > highestFreq)
                {
                    highestFreq = freq;
                }
                frequencies[word] = freq;
            }
            return frequencies.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
        }

        public int TimeNeededToAnswerDynamic(IQueryable<FullView> query)
        {
            var list = query.ToList();

            Dictionary<DateTime, int> dictionary = new Dictionary<DateTime, int>();
            var sameTime = new Dictionary<string, string>();
            for (int index = 0; index < list.Count; index++)
            {
                var content = new StringBuilder();
                for (int i = index; i < list.Count; i++)
                {
                    if (list[index].dateTime == list[i].dateTime)
                    {
                        content.Append(list[i].content + " ");

                    }
                    else
                    {

                    }
                }
            }
            return 0;
        }

        public int numberOfAllMessagesByUsers(int idOfConversation)
            => new DatabaseNew().FullViews.Count(x => x.conversationID == idOfConversation);

        public int numberOfAllMessagesByUser(int idOfConversation, string nameConversonalist)
            => new DatabaseNew().FullViews.Count(
                x => x.personName == nameConversonalist && x.conversationID == idOfConversation);

        public double PercentageMessagesPerConversation(int idOfConversation, string nameConversonalist)
            =>
                (double)
                    (numberOfAllMessagesByUser(idOfConversation, nameConversonalist)/
                     numberOfAllMessagesByUsers(idOfConversation))*100;


        public string MostFrequentlyUsedInsult(int idOfConversation, string nameConversonalist, string param0,
            string param1, string param2, string param3, string param4, string param5, string param6) =>
                MostFrequentlyUsedWord(
                    new DatabaseNew().FullViews.Where(
                        x =>
                            x.personName == nameConversonalist && x.conversationID == idOfConversation && (
                            x.content.Contains(param0) || x.content.Contains(param1) ||
                            x.content.Contains(param2) || x.content.Contains(param3) ||
                            x.content.Contains(param4) || x.content.Contains(param5) ||
                            x.content.Contains(param6)))).Keys.ElementAt(0);

        /// <summary>
        /// Most Frequently Used Insult (Number of Insults).
        /// </summary>
        /// <param name="idOfConversation"></param>
        /// <param name="nameConversonalist"></param>
        /// <param name="param0"></param>
        /// <param name="param1"></param>
        /// <param name="param2"></param>
        /// <param name="param3"></param>
        /// <param name="param4"></param>
        /// <param name="param5"></param>
        /// <param name="param6"></param>
        /// <returns></returns>
        public int MostFrequentlyUsedInsultNumber(int idOfConversation, string nameConversonalist, string param0,
            string param1, string param2, string param3, string param4, string param5, string param6) =>
                MostFrequentlyUsedWord(
                    new DatabaseNew().FullViews.Where(
                       x =>
                            x.personName == nameConversonalist && x.conversationID == idOfConversation && (
                            x.content.Contains(param0) || x.content.Contains(param1) ||
                            x.content.Contains(param2) || x.content.Contains(param3) ||
                            x.content.Contains(param4) || x.content.Contains(param5) ||
                            x.content.Contains(param6)))).Values.ElementAt(0);

        public virtual int DaysFromFirstDayToLast(Dictionary<DateTime, int> dictionary)
        {
            DateTime firstDay = dictionary.Select(x => x.Key).ElementAt(0);

            DateTime lastDay = dictionary.Last().Key;

            return lastDay.Subtract(firstDay).Days;
        }


    }

    public static class AnswersFactory
    {

        private static IAnswersCalculate answers = null;

        public static IAnswersCalculate Create()
        {
            return answers ?? new AnswersCalculate();
        }

        public static void SetInstance(IAnswersCalculate _answers)
        {
            answers = _answers;
        }
    }

}
