﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Xml;
using System.Xml.Linq;
using MessagesParserNAnalyzer.Code.Models.EntityFramework;
using MessagesParserNAnalyzer.Properties;

namespace MessagesParserNAnalyzer.Code.Forms.AnalyseForm
{
    //public class Siekiera<T, T1>
    //{
    //    public T Key { get; set; }
    //    public T1 Value { get; set; }

    //    public static void ConvertThatShit<T, T1>(string path)
    //    {
    //        XElement xElem2 = XElement.Load(new XmlTextReader(@path));

    //        var abc = xElem2.Descendants("item").FirstOrDefault().Attribute("Key").Value;

    //        TypeConverter typeConverter = TypeDescriptor.GetConverter(typeof(T));
    //        object propValue = typeConverter.ConvertFromString(abc);

           
    //        //return xElem2.Descendants("item").Select(x => 
    //        //new Siekiera()
    //        //{
    //        //    Key = x.Attribute("Key").Value,
    //        //    Value = (T1)x.Attribute("Value").Value
    //        //}).ToList();
    //    }
    //}

    public partial class Analyse : Form, IAnswersCalculate
    {
        #region Fields

        private readonly IAnswersCalculate _answersCalculateImplementation;

        private List<string> conversonalists;

        #endregion

        #region Properties

        public Dictionary<string, int> dictionaryOfConversationsAndNumberOfMessagesFromXML { get; private set; } =
            new Dictionary<string, int>();

        public Dictionary<DateTime, int> MessagesPerDayPerson { get; set; } = new Dictionary<DateTime, int>();

        public List<string> Conversonalists
        {
            get { return conversonalists; }
            set
            {
                conversonalists.Clear();
                conversonalists.AddRange(value);
            }
        }
        #endregion

        #region C'Tor
        public Analyse(IAnswersCalculate answersCalculateImplementation)
        {
            _answersCalculateImplementation = answersCalculateImplementation;
            InitializeComponent();
            CbEntryData();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Entry parameters for combo boxes 
        /// </summary>
        private void CbEntryData()
        {
            var criticalpath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Analyser of Conversation\Conversations";
            
            var conversationPath = criticalpath + @"\Conversations.xml";

           
            using (var context = new DatabaseNew())
            {
                if (File.Exists(conversationPath))
                {


                    dictionaryOfConversationsAndNumberOfMessagesFromXML.DeserializeDictionary(conversationPath);

                    cbConversations.Items.Clear();

                    foreach (var dictionaryOfMessage in dictionaryOfConversationsAndNumberOfMessagesFromXML)
                    {
                        var str = "(" + dictionaryOfMessage.Value +
                                  ")" + " " + dictionaryOfMessage.Key;
                        cbConversations.Items.Add(str);
                    }

                }
                else
                {
                    foreach (
                        var conversation in context.Conversations.Where(x => x.conversationName.Contains("Rużanowski"))
                    )
                    {
                        var countOfMessages = conversation.Messages.Count(x => x.conversationID == conversation.id);

                        if (countOfMessages < 100)
                            continue;

                        var str = conversation.conversationName.Replace("Rużanowski Sebastian,", "")
                                      .Replace("Sebastian Rużanowski,", "")
                                      .Replace("Rużanowski Sebastian", "")
                                      .Replace("Sebastian Rużanowski", "").Trim()
                                  + " id: " + conversation.id;

                        dictionaryOfConversationsAndNumberOfMessagesFromXML.Add(str, countOfMessages);
                    }
                    cbConversations.Items.Clear();

                    dictionaryOfConversationsAndNumberOfMessagesFromXML =
                        dictionaryOfConversationsAndNumberOfMessagesFromXML.OrderByDescending(x => x.Value)
                            .ToDictionary(x => x.Key, x => x.Value);

                    foreach (var dictionaryOfMessage in dictionaryOfConversationsAndNumberOfMessagesFromXML)
                    {
                        var str = "(" + dictionaryOfMessage.Value +
                                  ")" + " " + dictionaryOfMessage.Key;
                        cbConversations.Items.Add(str);
                    }

                    dictionaryOfConversationsAndNumberOfMessagesFromXML.SerializeDictionary(conversationPath);

                }
            }
        }

        #endregion

        #region OnClick Events

        /// <summary>
        /// When selected some index in combo box Conversation SELECT
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbConversations_SelectedIndexChanged(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            cbAnalyseResults.Items.Clear();

            if (cbConversations.SelectedItem.ToString().Contains("id:"))
            {
                cbPerson.Enabled = true;

                var splitConversationChose = cbConversations.SelectedItem.ToString().Split(' ').ToList();

                var indexOfId = splitConversationChose.FindIndex(x => x == "id:");
                var idOfConversation = int.Parse(splitConversationChose[indexOfId + 1]);

                using (var context = new DatabaseNew())
                {
                    progressBar1.PerformStep();

                    conversonalists =
                        context.PeopleInConversations.Where(x => x.id == idOfConversation && !x.personName.Contains("."))
                            .Select(x => x.personName)
                            .Distinct().ToList();

                    progressBar1.PerformStep();
                    cbPerson.Items.Clear();
                    cbPerson.Items.AddRange(conversonalists.ToArray());

                    #region chart

                    #region setting Chart 

                    var messagesPerDay =
                        context.FullViews.Where(
                            x =>
                                    x.conversationID == idOfConversation);


                    chartMessages.Series.Clear();

                    chartMessages.Palette = ChartColorPalette.Pastel;
                    chartMessages.ChartAreas[0].AxisX.LabelStyle.Format = "yyyy-MM";
                    chartMessages.ChartAreas[0].AxisX.Interval = 1;
                    chartMessages.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Months;
                    chartMessages.ChartAreas[0].AxisX.IntervalOffset = 1;
                    chartMessages.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
                    chartMessages.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;

                    #endregion

                    cbChartConversonalists.Items.Clear();
                    for (var i = 0; i < conversonalists.Count; i++)
                    {
                        MessagesPerDayPerson.Clear();
                        var personLinqAble = conversonalists[i];

                        var msgs =
                            messagesPerDay.Where(x => x.personName == personLinqAble).ToList();

                        chartMessages.Series.Add(conversonalists[i]);
                        chartMessages.Series[conversonalists[i]].XValueType = ChartValueType.Date;
                        chartMessages.Series[conversonalists[i]].ChartType = SeriesChartType.Line;

                        var criticalpath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                           @"\Analyser of Conversation\Conversations";

                        var messagesPerDayPersonPath = criticalpath  + @"\" + idOfConversation +
                                                       @"\" + personLinqAble + @"\messagesPerDay"+
                                                       ".xml";
                        //var conversonalistsPath = criticalpath + criticalpath + @"\MessagesPerDay" + @"\" + idOfConversation +
                        //                      @"\" + "Conversonalists.xml";

                    
                    if (!File.Exists(messagesPerDayPersonPath))
                        {
                            foreach (var fullView in msgs)
                            {
                                if (!MessagesPerDayPerson.ContainsKey(fullView.dateTime.Date))
                                    MessagesPerDayPerson.Add(fullView.dateTime.Date, 1);
                                else
                                {
                                    MessagesPerDayPerson[fullView.dateTime.Date] += 1;
                                }
                            }
                            MessagesPerDayPerson = MessagesPerDayPerson.OrderBy(x => x.Key)
                                .ToDictionary(x => x.Key, x => x.Value);

                            var firstDay = MessagesPerDayPerson.ElementAt(0).Key.Date;
                            do
                            {

                                if (!MessagesPerDayPerson.ContainsKey(firstDay))
                                {
                                    MessagesPerDayPerson.Add(firstDay, 0);
                                }

                                firstDay = firstDay.AddDays(1);
                            } while (firstDay != DateTime.Now.Date);

                            MessagesPerDayPerson = MessagesPerDayPerson.OrderBy(x => x.Key)
                                .ToDictionary(x => x.Key, x => x.Value);

                            MessagesPerDayPerson.SerializeDictionary(messagesPerDayPersonPath);


                        }
                        else
                        {
                            MessagesPerDayPerson.DeserializeDictionary(messagesPerDayPersonPath);
                        }
                        
                        foreach (var keyPair in MessagesPerDayPerson)
                        {
                            chartMessages.Series[personLinqAble].Points.AddXY(keyPair.Key.ToOADate(), keyPair.Value);
                        }
                        cbChartConversonalists.Items.Add(conversonalists[i], true);
                    }
                    progressBar1.PerformStep();

                 
                    
                    //TODO: DODAC TIME NEEDED TO ANSWER,
                    //TODO: NOWE CHARTY, 

                    cbSelectChartColor.Enabled = cbSelectChartType.Enabled = cbPerson.Enabled = true;

                    #endregion
                }
            }
            else
            {
                cbPerson.Enabled = false;
            }
            progressBar1.PerformStep();
        }

        /// <summary>
        /// When selected some different index in combo box Person SELECT
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbPerson_SelectedIndexChanged(object sender, EventArgs e)
        {


            int numberOfDaysConversation = 0;
            int numberOfDaysPerson = 0;
            int numberOfAllMessagesByUsers = 0;
            int numberOfAllMessagesByUser;
            int mostFrequentlyUsedInsultNumber = 0;
            string mostFrequentlyUsedInsult;
            string pathMessagesPerPerson;
            string pathMessagesPerConversation;
            float averageMessagesPerDayByEveryPerson = 0;
            float averageMessagesPerDayByPerson = 0;
            double percentageMessagesPerConversation;
            List<string> splitConversationChosen2 = cbConversations.SelectedItem.ToString().Split(' ').ToList();
            Dictionary<string, int> mostFrequentlyUsedWords = new Dictionary<string, int>();
            Dictionary<DateTime, int> MessagesPerDayConversation = new Dictionary<DateTime, int>();
            IEnumerable<KeyValuePair<string, int>> mostFrequentlyUsedWordsShort;
            IEnumerable<KeyValuePair<string, int>> mostFrequentlyUsedWordsMedium;
            IEnumerable<KeyValuePair<string, int>> frequentlyUsedWords2;
            IEnumerable<KeyValuePair<string, int>> mostFrequentlyUsedWordsLong;
            IQueryable<FullView> msgPerDayConversation;
            IQueryable<FullView> msgPerDayPerson;

            int indexOfId;

            int idOfConversation;

            var basePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Analyser of Conversation\Conversations";
            using (var context = new DatabaseNew())
            {
                #region prepare

                indexOfId = splitConversationChosen2.FindIndex(x => x == "id:");
                idOfConversation = int.Parse(splitConversationChosen2[indexOfId + 1]);
                ExtensionMethods.ClearUsedWords();
                progressBar1.Value = 0;
                cbAnalyseResults.Enabled = true;

                #endregion

                #region calculate

                progressBar1.PerformStep();


                numberOfAllMessagesByUsers = this.numberOfAllMessagesByUsers(idOfConversation);

                numberOfAllMessagesByUser = this.numberOfAllMessagesByUser(idOfConversation,
                    ((ComboBox) sender).SelectedItem.ToString());

                percentageMessagesPerConversation = numberOfAllMessagesByUser/
                                                    (double) numberOfAllMessagesByUsers*
                                                    100;

                mostFrequentlyUsedWords = MostFrequentlyUsedWord(new DatabaseNew().FullViews.Where(
                        x =>
                            x.personName == ((ComboBox)sender).SelectedItem &&
                            x.conversationID == idOfConversation)

                    );


                progressBar1.PerformStep();

                frequentlyUsedWords2 =
                    mostFrequentlyUsedWords.Where(x => x.Key.Contains(ref Forbidden.forbiddenSymbols, false) != true);

                var usedWords2 = frequentlyUsedWords2 as IList<KeyValuePair<string, int>> ??
                                 frequentlyUsedWords2.ToList();

                mostFrequentlyUsedWordsShort = usedWords2.Where(
                    x =>
                        x.Key.Length >= 4 && x.Key.Length < 7 &&
                        x.Key.Contains(ref Forbidden.forbiddenWords) != true
                );
                mostFrequentlyUsedWordsMedium = usedWords2.Where(
                    x =>
                        x.Key.Length >= 7 && x.Key.Length < 10 &&
                        x.Key.Contains(ref Forbidden.forbiddenWords) != true
                );
                mostFrequentlyUsedWordsLong = usedWords2.Where(
                    x =>
                            x.Key.Length >= 10 && x.Key.Contains(ref Forbidden.forbiddenWords) != true
                );

                mostFrequentlyUsedInsult = mostFrequentlyUsedWords.FirstOrDefault(
                    x =>
                            x.Key.Contains(ref Forbidden.forbiddenWords, false)).Key;

                mostFrequentlyUsedInsultNumber = mostFrequentlyUsedWords.FirstOrDefault(
                    x =>
                            x.Key.Contains(ref Forbidden.forbiddenWords, false)).Value;


                msgPerDayConversation = context.FullViews.Where(
                    x =>
                            x.conversationID == idOfConversation);

                msgPerDayPerson = msgPerDayConversation.Where(x => x.personName == cbPerson.SelectedItem);

                MessagesPerDayPerson.Clear();

                pathMessagesPerConversation = basePath + @"\" + idOfConversation + @"\messagesPerConversation.xml";

                pathMessagesPerPerson = basePath + @"\" + idOfConversation + @"\"+ ((ComboBox) sender).SelectedItem  +@"\messagesPerPerson.xml";

                if (!File.Exists(pathMessagesPerConversation))
                {
                    MessagesPerDayConversation.CreateDictionary(msgPerDayConversation);
                }
                else
                {
                    MessagesPerDayConversation.DeserializeDictionary(pathMessagesPerConversation);
                }

                if (!File.Exists(pathMessagesPerPerson))
                {
                    MessagesPerDayPerson.CreateDictionary(msgPerDayPerson);
                }
                else
                {
                    MessagesPerDayPerson.DeserializeDictionary(pathMessagesPerPerson);
                }

                progressBar1.PerformStep();

                MessagesPerDayConversation = MessagesPerDayConversation.OrderBy(x => x.Key)
                    .ToDictionary(x => x.Key, x => x.Value);

                MessagesPerDayPerson = MessagesPerDayPerson.OrderBy(x => x.Key)
                    .ToDictionary(x => x.Key, x => x.Value);


                numberOfDaysConversation = MessagesPerDayConversation.ToList().Count;

                numberOfDaysPerson = MessagesPerDayPerson.ToList().Count;

                var daysWithoutResponidng = numberOfDaysConversation - numberOfDaysPerson;

                averageMessagesPerDayByEveryPerson = (float) numberOfAllMessagesByUsers/
                                                     (float) numberOfDaysConversation;

                averageMessagesPerDayByPerson = (float) numberOfAllMessagesByUser/
                                                (float) numberOfDaysPerson;


                DateTime firstDay = MessagesPerDayConversation.Select(x => x.Key).ElementAt(0);

                int daysFromFirstDayToLast = DaysFromFirstDayToLast(MessagesPerDayConversation);
                float constitutingPercentMsgPerDay = (float)
                                                     (averageMessagesPerDayByPerson/
                                                      (float) averageMessagesPerDayByEveryPerson)
                                                     *100;
                var timeNeeded =
                     new TimeClass();

                timeNeeded = timeNeeded.TimeNeededToAnswer(
                    new DatabaseNew().FullViews.Where(x => x.conversationID == idOfConversation),
                    conversonalists);

                List<string> lazyAdd = new List<string>();

                var TimeNeededToAnswerWholeConversation = (float) timeNeeded.czasOdpowiedzi.Sum();
                var TimeNeededToAnswerBigDelays = timeNeeded._bigDelay.Max(x => x.Key);
                var TimeNeededToAnswerBigDelaysNumber = timeNeeded._bigDelay[TimeNeededToAnswerBigDelays];

                var TimeChangingConversonalists = timeNeeded.dialogAvgCzasyOdp.Sum();

                lazyAdd.Add("");
                lazyAdd.Add("Average time that takes to response: "+TimeNeededToAnswerWholeConversation);
                lazyAdd.Add("Biggest delay in writing was on: " + TimeNeededToAnswerBigDelays.Date + ", that took " + TimeNeededToAnswerBigDelaysNumber +" mins");
                lazyAdd.Add("Average time between sending by turns" + TimeChangingConversonalists);
                lazyAdd.Add("");
                foreach (var conversonalist in conversonalists)
                {
                    lazyAdd.Add(conversonalist+" was starting the conversation "+timeNeeded.MostFrequentlyWrittenDayByPerson(timeNeeded.startOfConversation, conversonalist)+" times.");
                }
                #endregion



                #region results

                try
                {
                    var resultsList = new List<string>
                    {
                        "Person: " + ((ComboBox) sender).SelectedItem,
                        "All messages in conversation: " + numberOfAllMessagesByUsers,
                        "All messages by " + ((ComboBox) sender).SelectedItem + ": " + numberOfAllMessagesByUser,
                        "Constituting percent of messages by " + ((ComboBox) sender).SelectedItem + ": " +
                        percentageMessagesPerConversation.ToString("##.00") + "%",
                        "",
                        "Average messages per day per conversation: " +
                        averageMessagesPerDayByEveryPerson.ToString("##.##"),
                        "Average messages per day per person:" + averageMessagesPerDayByPerson.ToString("##.##"),
                        "Days of writing by every person: " + numberOfDaysConversation,
                        "Days of writing by person: " + numberOfDaysPerson,
                        "Constituting percent of messages per day:" + constitutingPercentMsgPerDay.ToString("##.##") +
                        "%",
                        "",
                        "First day of writing: " + firstDay.Date.ToString().Replace(@"00:00:00", ""),
                        "Days since first till last day: " + daysFromFirstDayToLast,
                        "Days without responding: " + daysWithoutResponidng,
                        "",


                        "Most frequently used short length word is: \"" + mostFrequentlyUsedWordsShort.ElementAt(0).Key +
                        "\" times used: " + mostFrequentlyUsedWordsShort.ElementAt(0).Value,
                        "Most frequently used short length word is: \"" + mostFrequentlyUsedWordsShort.ElementAt(1).Key +
                        "\" times used: " + mostFrequentlyUsedWordsShort.ElementAt(1).Value,
                        "Most frequently used short length word is: \"" + mostFrequentlyUsedWordsShort.ElementAt(2).Key +
                        "\" times used: " + mostFrequentlyUsedWordsShort.ElementAt(2).Value,
                        "Most frequently used short length word is: \"" + mostFrequentlyUsedWordsShort.ElementAt(3).Key +
                        "\" times used: " + mostFrequentlyUsedWordsShort.ElementAt(3).Value,
                        "",

                        "Most frequently used medium length word is: \"" +
                        mostFrequentlyUsedWordsMedium.ElementAt(0).Key +
                        "\" times used: " + mostFrequentlyUsedWordsMedium.ElementAt(0).Value,
                        "Most frequently used medium length word is: \"" +
                        mostFrequentlyUsedWordsMedium.ElementAt(1).Key +
                        "\" times used: " + mostFrequentlyUsedWordsMedium.ElementAt(1).Value,
                        "Most frequently used medium length word is: \"" +
                        mostFrequentlyUsedWordsMedium.ElementAt(2).Key +
                        "\" times used: " + mostFrequentlyUsedWordsMedium.ElementAt(2).Value,
                        "Most frequently used medium length word is: \"" +
                        mostFrequentlyUsedWordsMedium.ElementAt(3).Key +
                        "\" times used: " + mostFrequentlyUsedWordsMedium.ElementAt(3).Value,
                        "",

                        "Most frequently used medium long word is: \"" + mostFrequentlyUsedWordsLong.ElementAt(0).Key +
                        "\" times used: " + mostFrequentlyUsedWordsLong.ElementAt(0).Value,
                        "Most frequently used medium long word is: \"" + mostFrequentlyUsedWordsLong.ElementAt(1).Key +
                        "\" times used: " + mostFrequentlyUsedWordsLong.ElementAt(1).Value,
                        "Most frequently used medium long word is: \"" + mostFrequentlyUsedWordsLong.ElementAt(2).Key +
                        "\" times used: " + mostFrequentlyUsedWordsLong.ElementAt(2).Value,
                        "Most frequently used medium long word is: \"" + mostFrequentlyUsedWordsLong.ElementAt(3).Key +
                        "\" times used: " + mostFrequentlyUsedWordsLong.ElementAt(3).Value,
                        "",
                        "Most frequently used insult is: \"" + mostFrequentlyUsedInsult + "\" times used: " +
                        mostFrequentlyUsedInsultNumber,

                    };
                    resultsList.AddRange(lazyAdd);
                    cbAnalyseResults.Items.Clear();
                    cbAnalyseResults.Items.AddRange(items: resultsList.ToArray());
                }
                catch
                {
                    MessageBox.Show("Too few messages.");
                }

                #endregion
            }

            progressBar1.PerformStep();
        }

        private void cbListForChart1_SelectedValueChanged(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            for (int i = 0; i < Conversonalists.Count; i++)
            {
                chartMessages.Series[Conversonalists[i]].Enabled = cbChartConversonalists.GetItemChecked(i);
            }
            progressBar1.Value = progressBar1.Maximum;
            chartMessages.ChartAreas[0].RecalculateAxesScale();

        }

        private void chartMessages_Click(object sender, EventArgs e)
        {

        }

        private void Analyse_Load(object sender, EventArgs e)
        {
            cbChartConversonalists.BringToFront();
            cbEnableSpecifiedDate.Checked =
                dateTimePicker1.Enabled =
                    dateTimePicker2.Enabled = cbAnalyseResults.Enabled = cbSelectChartType.Enabled =
                        cbSelectChartColor.Enabled = cbPerson.Enabled = false;
            cbSelectChartType.Text = Resources.Analyse_Analyse_Load_Line;

        }

        private void CbChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (conversonalists == null)
                return;

            progressBar1.Value = 0;
            for (int i = 0; i < Conversonalists.Count; i++)
            {
                chartMessages.Series[Conversonalists[i]].ChartType =
                    (SeriesChartType) Enum.Parse(typeof(SeriesChartType), cbSelectChartType.SelectedItem.ToString());
            }
            progressBar1.Value = progressBar1.Maximum;
            chartMessages.ChartAreas[0].RecalculateAxesScale();
        }

        private void CbEnableSpecifiedDateCheckedChanged(object sender, EventArgs e)
        {
            dateTimePicker2.Enabled = dateTimePicker1.Enabled = cbEnableSpecifiedDate.Checked;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (conversonalists == null)
                return;

            progressBar1.Value = 0;
            foreach (string t in Conversonalists)
            {
                chartMessages.Series[t].Palette =
                    (ChartColorPalette)
                    Enum.Parse(typeof(ChartColorPalette), cbSelectChartColor.SelectedItem.ToString());
            }
            progressBar1.Value = progressBar1.Maximum;
            chartMessages.ChartAreas[0].RecalculateAxesScale();
        }

        #endregion

        #region delegates

        public Dictionary<string, int> MostFrequentlyUsedWord(IQueryable<FullView> query)
            => _answersCalculateImplementation.MostFrequentlyUsedWord(query);

        public int TimeNeededToAnswerDynamic(IQueryable<FullView> query)
            => _answersCalculateImplementation.TimeNeededToAnswerDynamic(query);

        public int numberOfAllMessagesByUsers(int idOfConversation)
            => _answersCalculateImplementation.numberOfAllMessagesByUsers(idOfConversation);

        public int numberOfAllMessagesByUser(int idOfConversation, string nameConversonalist)
            => _answersCalculateImplementation.numberOfAllMessagesByUser(idOfConversation, nameConversonalist);

        public double PercentageMessagesPerConversation(int idOfConversation, string nameConversonalist)
            => _answersCalculateImplementation.PercentageMessagesPerConversation(idOfConversation, nameConversonalist);

        public string MostFrequentlyUsedInsult(int idOfConversation, string nameConversonalist, string param0,
                string param1,
                string param2, string param3, string param4, string param5, string param6)
            =>
            _answersCalculateImplementation.MostFrequentlyUsedInsult(idOfConversation, nameConversonalist, param0,
                param1, param2, param3, param4, param5, param6);

        public int MostFrequentlyUsedInsultNumber(int idOfConversation, string nameConversonalist, string param0,
                string param1,
                string param2, string param3, string param4, string param5, string param6)
            =>
            _answersCalculateImplementation.MostFrequentlyUsedInsultNumber(idOfConversation, nameConversonalist,
                param0, param1, param2, param3, param4, param5, param6);

        public int DaysFromFirstDayToLast(Dictionary<DateTime, int> dictionary)
            => _answersCalculateImplementation.DaysFromFirstDayToLast(dictionary);

        #endregion

    }
}
