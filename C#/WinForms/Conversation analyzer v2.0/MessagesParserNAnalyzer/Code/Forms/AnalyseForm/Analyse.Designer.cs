﻿namespace MessagesParserNAnalyzer.Code.Forms.AnalyseForm
{
    partial class Analyse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chartMessages = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cbChartConversonalists = new System.Windows.Forms.CheckedListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbProgressBar = new System.Windows.Forms.GroupBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.gbSelectChartColor = new System.Windows.Forms.GroupBox();
            this.cbSelectChartColor = new System.Windows.Forms.ComboBox();
            this.gbSelectChartType = new System.Windows.Forms.GroupBox();
            this.cbSelectChartType = new System.Windows.Forms.ComboBox();
            this.gbPickSpecifiedDate = new System.Windows.Forms.GroupBox();
            this.cbEnableSpecifiedDate = new System.Windows.Forms.CheckBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.gbConversationPersonChoice = new System.Windows.Forms.GroupBox();
            this.cbAnalyseResults = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPerson = new System.Windows.Forms.ComboBox();
            this.cbConversations = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.chartMessages)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gbProgressBar.SuspendLayout();
            this.gbSelectChartColor.SuspendLayout();
            this.gbSelectChartType.SuspendLayout();
            this.gbPickSpecifiedDate.SuspendLayout();
            this.gbConversationPersonChoice.SuspendLayout();
            this.SuspendLayout();
            // 
            // chartMessages
            // 
            chartArea3.Name = "ChartArea1";
            this.chartMessages.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chartMessages.Legends.Add(legend3);
            this.chartMessages.Location = new System.Drawing.Point(6, 77);
            this.chartMessages.Name = "chartMessages";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chartMessages.Series.Add(series3);
            this.chartMessages.Size = new System.Drawing.Size(773, 299);
            this.chartMessages.TabIndex = 6;
            this.chartMessages.Text = "chart1";
            this.chartMessages.Click += new System.EventHandler(this.chartMessages_Click);
            // 
            // cbChartConversonalists
            // 
            this.cbChartConversonalists.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbChartConversonalists.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cbChartConversonalists.CheckOnClick = true;
            this.cbChartConversonalists.FormattingEnabled = true;
            this.cbChartConversonalists.Location = new System.Drawing.Point(638, 271);
            this.cbChartConversonalists.Name = "cbChartConversonalists";
            this.cbChartConversonalists.Size = new System.Drawing.Size(141, 105);
            this.cbChartConversonalists.TabIndex = 8;
            this.cbChartConversonalists.SelectedValueChanged += new System.EventHandler(this.cbListForChart1_SelectedValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gbProgressBar);
            this.groupBox1.Controls.Add(this.gbSelectChartColor);
            this.groupBox1.Controls.Add(this.cbChartConversonalists);
            this.groupBox1.Controls.Add(this.gbSelectChartType);
            this.groupBox1.Controls.Add(this.gbPickSpecifiedDate);
            this.groupBox1.Controls.Add(this.chartMessages);
            this.groupBox1.Location = new System.Drawing.Point(600, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(798, 386);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // gbProgressBar
            // 
            this.gbProgressBar.Controls.Add(this.progressBar1);
            this.gbProgressBar.Location = new System.Drawing.Point(6, 15);
            this.gbProgressBar.Name = "gbProgressBar";
            this.gbProgressBar.Size = new System.Drawing.Size(111, 49);
            this.gbProgressBar.TabIndex = 17;
            this.gbProgressBar.TabStop = false;
            this.gbProgressBar.Text = "Progress Bar";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(6, 20);
            this.progressBar1.Maximum = 4;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(93, 23);
            this.progressBar1.Step = 1;
            this.progressBar1.TabIndex = 11;
            // 
            // gbSelectChartColor
            // 
            this.gbSelectChartColor.Controls.Add(this.cbSelectChartColor);
            this.gbSelectChartColor.Location = new System.Drawing.Point(286, 16);
            this.gbSelectChartColor.Name = "gbSelectChartColor";
            this.gbSelectChartColor.Size = new System.Drawing.Size(110, 48);
            this.gbSelectChartColor.TabIndex = 11;
            this.gbSelectChartColor.TabStop = false;
            this.gbSelectChartColor.Text = "Select Chart Color";
            // 
            // cbSelectChartColor
            // 
            this.cbSelectChartColor.FormattingEnabled = true;
            this.cbSelectChartColor.Items.AddRange(new object[] {
            "None",
            "Bright",
            "Grayscale",
            "Excel",
            "Light",
            "Pastel",
            "EarthTones",
            "SemiTransparent",
            "Berry",
            "Chocolate",
            "Fire",
            "SeaGreen",
            "BrightPastel"});
            this.cbSelectChartColor.Location = new System.Drawing.Point(6, 18);
            this.cbSelectChartColor.Name = "cbSelectChartColor";
            this.cbSelectChartColor.Size = new System.Drawing.Size(98, 21);
            this.cbSelectChartColor.TabIndex = 0;
            this.cbSelectChartColor.Text = "Pastel";
            this.cbSelectChartColor.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // gbSelectChartType
            // 
            this.gbSelectChartType.Controls.Add(this.cbSelectChartType);
            this.gbSelectChartType.Location = new System.Drawing.Point(126, 16);
            this.gbSelectChartType.Name = "gbSelectChartType";
            this.gbSelectChartType.Size = new System.Drawing.Size(154, 50);
            this.gbSelectChartType.TabIndex = 16;
            this.gbSelectChartType.TabStop = false;
            this.gbSelectChartType.Text = "Select Chart Type";
            // 
            // cbSelectChartType
            // 
            this.cbSelectChartType.FormattingEnabled = true;
            this.cbSelectChartType.Items.AddRange(new object[] {
            "Area",
            "FastLine",
            "FastPoint",
            "Line",
            "Point"});
            this.cbSelectChartType.Location = new System.Drawing.Point(6, 19);
            this.cbSelectChartType.Name = "cbSelectChartType";
            this.cbSelectChartType.Size = new System.Drawing.Size(142, 21);
            this.cbSelectChartType.TabIndex = 10;
            this.cbSelectChartType.Text = "Line";
            this.cbSelectChartType.SelectedValueChanged += new System.EventHandler(this.CbChartType_SelectedIndexChanged);
            // 
            // gbPickSpecifiedDate
            // 
            this.gbPickSpecifiedDate.Controls.Add(this.cbEnableSpecifiedDate);
            this.gbPickSpecifiedDate.Controls.Add(this.dateTimePicker2);
            this.gbPickSpecifiedDate.Controls.Add(this.dateTimePicker1);
            this.gbPickSpecifiedDate.Location = new System.Drawing.Point(402, 16);
            this.gbPickSpecifiedDate.Name = "gbPickSpecifiedDate";
            this.gbPickSpecifiedDate.Size = new System.Drawing.Size(377, 50);
            this.gbPickSpecifiedDate.TabIndex = 15;
            this.gbPickSpecifiedDate.TabStop = false;
            this.gbPickSpecifiedDate.Text = "Pick Specified Date";
            // 
            // cbEnableSpecifiedDate
            // 
            this.cbEnableSpecifiedDate.AutoSize = true;
            this.cbEnableSpecifiedDate.Location = new System.Drawing.Point(6, 20);
            this.cbEnableSpecifiedDate.Name = "cbEnableSpecifiedDate";
            this.cbEnableSpecifiedDate.Size = new System.Drawing.Size(59, 17);
            this.cbEnableSpecifiedDate.TabIndex = 18;
            this.cbEnableSpecifiedDate.Text = "Enable";
            this.cbEnableSpecifiedDate.UseVisualStyleBackColor = true;
            this.cbEnableSpecifiedDate.CheckedChanged += new System.EventHandler(this.CbEnableSpecifiedDateCheckedChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(222, 20);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(145, 20);
            this.dateTimePicker2.TabIndex = 17;
            this.dateTimePicker2.Value = new System.DateTime(2016, 9, 7, 23, 44, 54, 0);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(71, 19);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(145, 20);
            this.dateTimePicker1.TabIndex = 15;
            this.dateTimePicker1.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // gbConversationPersonChoice
            // 
            this.gbConversationPersonChoice.Controls.Add(this.cbAnalyseResults);
            this.gbConversationPersonChoice.Controls.Add(this.label2);
            this.gbConversationPersonChoice.Controls.Add(this.label1);
            this.gbConversationPersonChoice.Controls.Add(this.cbPerson);
            this.gbConversationPersonChoice.Controls.Add(this.cbConversations);
            this.gbConversationPersonChoice.Location = new System.Drawing.Point(0, 12);
            this.gbConversationPersonChoice.Name = "gbConversationPersonChoice";
            this.gbConversationPersonChoice.Size = new System.Drawing.Size(594, 386);
            this.gbConversationPersonChoice.TabIndex = 11;
            this.gbConversationPersonChoice.TabStop = false;
            this.gbConversationPersonChoice.Text = "Pick Conversation & Person";
            // 
            // cbAnalyseResults
            // 
            this.cbAnalyseResults.FormattingEnabled = true;
            this.cbAnalyseResults.Location = new System.Drawing.Point(11, 99);
            this.cbAnalyseResults.Name = "cbAnalyseResults";
            this.cbAnalyseResults.Size = new System.Drawing.Size(566, 277);
            this.cbAnalyseResults.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Person";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Conversation";
            // 
            // cbPerson
            // 
            this.cbPerson.FormattingEnabled = true;
            this.cbPerson.Location = new System.Drawing.Point(11, 72);
            this.cbPerson.Name = "cbPerson";
            this.cbPerson.Size = new System.Drawing.Size(566, 21);
            this.cbPerson.TabIndex = 7;
            this.cbPerson.SelectedIndexChanged += new System.EventHandler(this.cbPerson_SelectedIndexChanged);
            // 
            // cbConversations
            // 
            this.cbConversations.FormattingEnabled = true;
            this.cbConversations.Location = new System.Drawing.Point(11, 32);
            this.cbConversations.Name = "cbConversations";
            this.cbConversations.Size = new System.Drawing.Size(566, 21);
            this.cbConversations.TabIndex = 6;
            this.cbConversations.SelectedIndexChanged += new System.EventHandler(this.cbConversations_SelectedIndexChanged);
            // 
            // Analyse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1406, 401);
            this.Controls.Add(this.gbConversationPersonChoice);
            this.Controls.Add(this.groupBox1);
            this.Name = "Analyse";
            this.Load += new System.EventHandler(this.Analyse_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartMessages)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.gbProgressBar.ResumeLayout(false);
            this.gbSelectChartColor.ResumeLayout(false);
            this.gbSelectChartType.ResumeLayout(false);
            this.gbPickSpecifiedDate.ResumeLayout(false);
            this.gbPickSpecifiedDate.PerformLayout();
            this.gbConversationPersonChoice.ResumeLayout(false);
            this.gbConversationPersonChoice.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataVisualization.Charting.Chart chartMessages;
        private System.Windows.Forms.CheckedListBox cbChartConversonalists;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbSelectChartType;
        private System.Windows.Forms.GroupBox gbSelectChartType;
        private System.Windows.Forms.GroupBox gbPickSpecifiedDate;
        private System.Windows.Forms.CheckBox cbEnableSpecifiedDate;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox gbSelectChartColor;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.GroupBox gbProgressBar;
        private System.Windows.Forms.GroupBox gbConversationPersonChoice;
        private System.Windows.Forms.ListBox cbAnalyseResults;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cbPerson;
        public System.Windows.Forms.ComboBox cbConversations;
        private System.Windows.Forms.ComboBox cbSelectChartColor;
    }
}