﻿namespace MessagesParserNAnalyzer
{
    partial class HtmlSplitter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSplitRows = new System.Windows.Forms.Button();
            this.txtBoxSplitRowsPath = new System.Windows.Forms.TextBox();
            this.btnSplitFiles = new System.Windows.Forms.Button();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.txtBoxSplitFilesPath = new System.Windows.Forms.TextBox();
            this.btnAddToDatabase = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnForm2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSplitRows
            // 
            this.btnSplitRows.Location = new System.Drawing.Point(281, 17);
            this.btnSplitRows.Name = "btnSplitRows";
            this.btnSplitRows.Size = new System.Drawing.Size(75, 23);
            this.btnSplitRows.TabIndex = 0;
            this.btnSplitRows.Text = "Split rows";
            this.btnSplitRows.UseVisualStyleBackColor = true;
            this.btnSplitRows.Click += new System.EventHandler(this.btnPathSelector_Click);
            // 
            // txtBoxSplitRowsPath
            // 
            this.txtBoxSplitRowsPath.Enabled = false;
            this.txtBoxSplitRowsPath.Location = new System.Drawing.Point(6, 19);
            this.txtBoxSplitRowsPath.Name = "txtBoxSplitRowsPath";
            this.txtBoxSplitRowsPath.Size = new System.Drawing.Size(269, 20);
            this.txtBoxSplitRowsPath.TabIndex = 1;
            this.txtBoxSplitRowsPath.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnSplitFiles
            // 
            this.btnSplitFiles.Location = new System.Drawing.Point(281, 45);
            this.btnSplitFiles.Name = "btnSplitFiles";
            this.btnSplitFiles.Size = new System.Drawing.Size(75, 23);
            this.btnSplitFiles.TabIndex = 2;
            this.btnSplitFiles.Text = "Split Files";
            this.btnSplitFiles.UseVisualStyleBackColor = true;
            this.btnSplitFiles.Click += new System.EventHandler(this.SplitFiles);
            // 
            // listBoxLog
            // 
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(6, 75);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(485, 160);
            this.listBoxLog.TabIndex = 3;
            // 
            // txtBoxSplitFilesPath
            // 
            this.txtBoxSplitFilesPath.Enabled = false;
            this.txtBoxSplitFilesPath.Location = new System.Drawing.Point(6, 48);
            this.txtBoxSplitFilesPath.Name = "txtBoxSplitFilesPath";
            this.txtBoxSplitFilesPath.Size = new System.Drawing.Size(269, 20);
            this.txtBoxSplitFilesPath.TabIndex = 4;
            // 
            // btnAddToDatabase
            // 
            this.btnAddToDatabase.Location = new System.Drawing.Point(362, 15);
            this.btnAddToDatabase.Name = "btnAddToDatabase";
            this.btnAddToDatabase.Size = new System.Drawing.Size(129, 54);
            this.btnAddToDatabase.TabIndex = 7;
            this.btnAddToDatabase.Text = "Parse To Db";
            this.btnAddToDatabase.UseVisualStyleBackColor = true;
            this.btnAddToDatabase.Click += new System.EventHandler(this.btnAddToDatabase_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAddToDatabase);
            this.groupBox1.Controls.Add(this.listBoxLog);
            this.groupBox1.Controls.Add(this.txtBoxSplitRowsPath);
            this.groupBox1.Controls.Add(this.btnSplitRows);
            this.groupBox1.Controls.Add(this.txtBoxSplitFilesPath);
            this.groupBox1.Controls.Add(this.btnSplitFiles);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(504, 254);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Splitter";
            // 
            // btnForm2
            // 
            this.btnForm2.Location = new System.Drawing.Point(217, 272);
            this.btnForm2.Name = "btnForm2";
            this.btnForm2.Size = new System.Drawing.Size(85, 53);
            this.btnForm2.TabIndex = 9;
            this.btnForm2.Text = "Analyse";
            this.btnForm2.UseVisualStyleBackColor = true;
            this.btnForm2.Click += new System.EventHandler(this.btnForm2_Click);
            // 
            // HtmlSplitter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 348);
            this.Controls.Add(this.btnForm2);
            this.Controls.Add(this.groupBox1);
            this.Name = "HtmlSplitter";
            this.Text = "HtmlSplitter & Parser To Database";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSplitRows;
        private System.Windows.Forms.TextBox txtBoxSplitRowsPath;
        private System.Windows.Forms.Button btnSplitFiles;
        private System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.TextBox txtBoxSplitFilesPath;
        private System.Windows.Forms.Button btnAddToDatabase;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnForm2;
    }
}

