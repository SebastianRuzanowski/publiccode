﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Transactions;
using System.Web.ModelBinding;
using System.Windows.Forms;
using HtmlAgilityPack;
using EntityFramework.BulkInsert.Extensions;
using MessagesParserNAnalyzer.Code.Forms.AnalyseForm;
using MessagesParserNAnalyzer.Code.Models.EntityFramework;
using MessagesParserNAnalyzer.Properties;
using Message = MessagesParserNAnalyzer.Code.Models.EntityFramework.Message;

namespace MessagesParserNAnalyzer
{
    public partial class HtmlSplitter : Form
    {
        List<string> listOfDestinations = new List<string>();
        public HtmlSplitter()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnPathSelector_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog
                {
                    Filter = @"htm files (*.htm)|*.htm",
                    InitialDirectory = @"C:\",
                    Title = Resources.HtmlSplitter_btnPathSelector_Click_Please_select_an_image_file_to_encrypt_
                };


                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    var filepath = Path.GetFullPath(dialog.FileName);
                    var listToAdd = new List<string>();
                    using (FileStream fs = File.Open(filepath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    using (BufferedStream bs = new BufferedStream(fs))
                    using (StreamReader sr = new StreamReader(bs))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                             listToAdd.AddRange(line.Replace("><", ">\n<").Replace("> <",">\n<").Split('\n'));

                            
                        }
                    }
                    string path = @"C:\Users\RS\Desktop\messages2.htm";
                    File.AppendAllLines(path, listToAdd);
                    MessageBox.Show(Resources.HtmlSplitter_btnPathSelector_Click_Rows_has_been_separated_);
                }
            }
            catch
            {
                // ignored
            }
        }

        private void SplitFiles(object sender, EventArgs e)
        {
            string filepath = @txtBoxSplitFilesPath.Text;

            var firstline = "<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\r\n<title>Rużanowski Sebastian - Wiadomości</title>\r\n<link rel=\"stylesheet\" href=\"../html/style.css\" type=\"text/css\" />\r\n</head>\r\n<body>";
            using (var fs = File.Open(filepath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var bs = new BufferedStream(fs))
            using (var sr = new StreamReader(bs))
            {
                int index = 1, indexFile = 1, indexDiv = 0;
                var listFirstLines = firstline.Split('\n');

                var listLastLines = new List<string>()
                {
                    "</body>",
                    "</html>"
                };
                string line;
                var list = new List<string>();
                while ((line = sr.ReadLine()) != null)
                {
                   if(line!= "<div>" || index<100)
                        list.Add(line);

                        if (line == "<div>")
                        {
                            
                            indexDiv++;
                            if (indexDiv%2 == 0)
                            {

                                list.AddRange(listLastLines);
                                var directorypath = @"C:\Users\RS\Desktop\MessagesFolder\messages";
                                var path = directorypath + indexFile.ToString() + @".htm";
                                File.AppendAllLines(path, list);
                                list.Clear();
                                if (indexFile > 0)
                                {
                                    list.InsertRange(0, listFirstLines);
                                list.Add("<div>");
                            }
                                else
                                {
                                    list.Add("<div>");
                                }
                                
                                listBoxLog.Items.Add("File messages" + indexFile.ToString() + ".htm" +
                                                     " has been added in: " + directorypath);
                                listOfDestinations.Add(path);
                            indexFile++;
                        }
                        }
                    index++;
                }
            }
            btnAddToDatabase.Enabled = true;

        }

        private void BtnSelectForSplitFiles_Click(object sender, EventArgs e)
        {
            try
            {

                OpenFileDialog dialog = new OpenFileDialog
                {
                    Filter = @"htm files (*.htm)|*.htm",
                    InitialDirectory = @"C:\",
                    Title = Resources.HtmlSplitter_btnPathSelector_Click_Please_select_an_image_file_to_encrypt_
                };


                btnSplitFiles.Enabled = dialog.ShowDialog() == DialogResult.OK;
                txtBoxSplitFilesPath.Text = Path.GetFullPath(dialog.FileName);

            }
            catch
            {
                MessageBox.Show(Resources.HtmlSplitter_BtnSelectForSplitFiles_Click_You_should_check_your_destination_folder_first_);
            }
        }

        private void btnSelectForSplitRows_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = @"htm files (*.htm)|*.htm",
                InitialDirectory = @"C:\",
                Title = Resources.HtmlSplitter_btnPathSelector_Click_Please_select_an_image_file_to_encrypt_
            };


            btnSplitRows.Enabled = dialog.ShowDialog() == DialogResult.OK;
            txtBoxSplitRowsPath.Text = Path.GetFullPath(dialog.FileName);
        }

        private void btnAddToDatabase_Click(object sender, EventArgs e)
        {
           var a = MessageBox.Show("Do you really want to parse to the database?", "Warning - parse to database",
                MessageBoxButtons.OKCancel);

            if (a == DialogResult.Cancel)
                return;
            else if (a == DialogResult.None)
                return;
           
            try
            {
                var watch = Stopwatch.StartNew();
                
               

                WebClient web = new WebClient();
                HtmlAgilityPack.HtmlDocument htmlAgility = new HtmlAgilityPack.HtmlDocument();

                var contents = new List<string>();

                var files = new List<string>();

                var personsList = new List<Person>();
                var conversationsList = new List<Conversation>();
                var messagesList = new List<Message>();

                var totalMessages = 0;
                var totalConversonalists = 0;
                var totalConversations = 0;

                string url = @"C:\Users\RS\Desktop\MessagesFolder";

                files.AddRange(Directory.GetFiles(url));
                var filesCount = files.Count;

                foreach (var file in files)
                {
                    htmlAgility.Load(web.OpenRead(file), Encoding.UTF8);

                    foreach (HtmlNode htmlNode in htmlAgility.DocumentNode.SelectNodes("//p"))
                    {

                        contents.Add(htmlNode.InnerText);
                    }

                    using (var context = new DatabaseNew())
                    {
                        using (var transactionScope = new TransactionScope(TransactionScopeOption.Required,
                                   new TimeSpan(0, 15, 0)))
                        {
                                
                            context.Database.CommandTimeout = 1000;
                            context.Configuration.AutoDetectChangesEnabled = false;
                            
                            #region class="message"

                            var numberOfMessages = 0;

                            foreach (var matchingDiv in
                                htmlAgility.DocumentNode.SelectNodes("//div[@class='message']"))
                            {
                                #region info
                                var info = Info(matchingDiv);
                                var conversationName = matchingDiv.ParentNode.FirstChild.InnerText;
                                var nameSender = info[0];
                                var dateMessage = Convert.ToDateTime(info[1]);
                                var content = contents[numberOfMessages];
                                #endregion

                                #region conceiving entities

                                if (personsList.All(x => x.id != HashGenerator(nameSender)))
                                {

                                    var personEntity = new Person()
                                    {
                                        id = HashGenerator(nameSender),
                                        personName = nameSender,
                                    };
                                    personsList.Add(personEntity);

                                }

                                if (conversationsList.All(x => x.id != HashGenerator(conversationName)))
                                {

                                    var conversationEntity = new Conversation()
                                    {
                                        id = HashGenerator(conversationName),
                                        conversationName = conversationName
                                    };

                                    conversationsList.Add(conversationEntity);
                                }
                                if (content != "" || content != null || content != "\n" || content != " " || content !="\r" || content !="\r\n")
                                {


                                    var messageEntity = new Message()
                                    {
                                        conversationID = HashGenerator(conversationName),
                                        personID = HashGenerator(nameSender),
                                        dateTime = dateMessage,
                                        content = content
                                    };
                                    messagesList.Add(messageEntity);
                                    totalMessages++;
                                }

                                #endregion

                                numberOfMessages++;
                                
                            }

                            #endregion
                            
                            #region Lack of people & conversations

                            var peopleToAdd = new List<Person>();
                            var conversationsToAdd = new List<Conversation>();
                            
                            foreach (var person in personsList)
                            {
                                bool personExist = context.People.Any(p => p.id.Equals(person.id));

                                if (!personExist)
                                {
                                    peopleToAdd.Add(person);
                                    totalConversonalists++;
                                }
                            }


                            foreach (var conversation in conversationsList)
                            {
                                bool conversationExist = context.Conversations.Any(p => p.id.Equals(conversation.id));

                                if (!conversationExist)
                                {
                                    conversationsToAdd.Add(conversation);
                                    totalConversations++;
                                }
                            }
                           
                            #endregion

                            context.BulkInsert(peopleToAdd);
                            context.BulkInsert(conversationsToAdd);
                            context.BulkInsert(messagesList);

                            messagesList.Clear();
                            contents.Clear();

                            context.SaveChanges();
                            transactionScope.Complete();
                        }

                }

                }
                watch.Stop();
                var elapsedTime = watch.ElapsedMilliseconds/1000;
                string resultOfOperation =
                    "Your conversation is kept in "
                     + filesCount +
                    " files.\n" + "You have been writing with " + totalConversonalists +
                    " people,\n" + "in " + totalConversations +
                    " conversations.\n" + totalMessages +
                    " has been proceeded in " + elapsedTime +
                    "s";
                
                MessageBox.Show(resultOfOperation);
            }
            catch
                (DbEntityValidationException
                    dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation(
                            $"Property: {validationError.PropertyName} Error: {validationError.ErrorMessage}");
                    }
                }
            }
        }   
       
        private int HashGenerator(string str)
        {
            int h = 0;
            for (int i = 0; i < str.Length; i++)
            {
                h = 15 * h + str[i];
            }
            return Math.Abs(h);
        
    }
        private List<string> Info(HtmlNode node)
        {
            var senderPersonAndDate = node.ChildNodes[1].InnerText;
            var splittedList = senderPersonAndDate.Replace("\r", "").Split('\n');
            var firstIndexOfContent = splittedList.ToList().FindIndex(x => x!="");
            var name = splittedList[firstIndexOfContent];
            var date = splittedList[firstIndexOfContent+1];
            var indexOfUtc = date.IndexOf(@"UTC+", StringComparison.Ordinal);
            date = date.Remove(indexOfUtc, 6);
            var splittedDate = date.Split(' ');
            if (splittedDate[0].Length == 1)
                splittedList[0].Insert(0, "0");
            splittedDate[3] = "";
            date = "";

            foreach (var s in splittedDate)
            {
                if (s != "")
                    date = date + " " + s;
            }


            return new List<string> { name, date, node.ChildNodes[1].OuterHtml };
        }

        private void btnForm2_Click(object sender, EventArgs e)
        {

            var answersCalculate = AnswersFactory.Create();

            var AnalyseForm = new Analyse(answersCalculate);
            AnalyseForm.Show();
        }
    }
}