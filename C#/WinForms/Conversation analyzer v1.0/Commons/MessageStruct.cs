﻿using System;

namespace Structures
{
    /// <summary>
    /// Struktura zawierająca wiadomość ID UŻYTKOWNIKA. IMIĘ. NAZWISKO. DATĘ WYSŁANIA.
    /// </summary>
    public struct MessageStruct
    {
        public string name;
        public DateTime dateTime;
        public string message;
        public string messageHTML;
        public MessageStruct(string name,DateTime dateTime,string message,string messageHTML)
        {
            this.name = name;
            this.dateTime = dateTime;
            this.message = message;
            this.messageHTML = messageHTML;
        }
    }
}
