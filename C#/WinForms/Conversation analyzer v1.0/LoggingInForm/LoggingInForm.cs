﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AnalyzerView.Interfaces;
using AnalyzerLogic;
using AnalyzerMainLogic;

namespace LoggingInForm
{
    public partial class LoggingInForm : Form, ILoggingIn
    {
        private readonly Presenter _presenter;
        public string Password
        {
            set { textBoxPassword.Text = value; }
        }

        public string Email
        {
            set { textBoxEmail.Text = value; }
        }
        public bool CloseLoadingPictureBox
        {
            set { Loading.Visible = value; LoggingMenu.Enabled = true; }
        }

        public void LoggedInLogic(bool loggedIn)
        {

            if (loggedIn)
            {
                label1.Text = "You are logged in.";
                btnLogIn.Enabled = false;
                textBoxEmail.Enabled = false;
                textBoxPassword.Enabled = false;
                labelLoggedIn.Text = "YOU ARE LOGGED IN.";
                labelLoggedIn.ForeColor = Color.LimeGreen;
                btnLogIn.Enabled = false;
                textBoxEmail.Enabled = false;
                textBoxPassword.Enabled = false;
                btnLogOut.Enabled = true;

            }
            else
            {
                label1.Text = "You are not logged in.";
                btnLogIn.Enabled = true;
                textBoxEmail.Enabled = true;
                textBoxPassword.Enabled = true;
                labelLoggedIn.Text = "PLEASE, LOG IN.";
                labelLoggedIn.ForeColor = Color.Crimson;
                btnLogIn.Enabled = true;
                textBoxEmail.Enabled = true;
                textBoxPassword.Enabled = true;

            }

        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            _presenter.LogOut();
        }

        public LoggingInForm(IAnalyzerView mainView)
        {
            InitializeComponent();
            LoggingMenu.Enabled = false;
            _presenter = new Presenter(mainView);
        }

        private void btnPathSelector_Click(object sender, EventArgs e)
        {
            string path = String.Empty;
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "Text|*.txt|All|*.*";
            if (file.ShowDialog() == DialogResult.OK)
            {
                path = file.FileName;
            }
            tbPath.Text = path;
            _presenter.PathOfLogins = path;
        }
        private void OnBtnLogInClick(object sender, EventArgs e)
        {
            if (textBoxEmail.Text == String.Empty)
                return;
            if (textBoxPassword.Text == String.Empty)
                return;
            _presenter.LogIn(textBoxEmail.Text, textBoxPassword.Text);
        }
        private void LoggingInForm_Load(object sender, EventArgs e)
        {

        }

        private void LoggingMenu_Enter(object sender, EventArgs e)
        {

        }
    }
}
