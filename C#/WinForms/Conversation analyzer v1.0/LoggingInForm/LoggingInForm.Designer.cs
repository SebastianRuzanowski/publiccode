﻿namespace LoggingInForm
{
    partial class LoggingInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoggingMenu = new System.Windows.Forms.GroupBox();
            this.labelLoggedIn = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPathSelector = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Loading = new System.Windows.Forms.PictureBox();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLogIn = new System.Windows.Forms.Button();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.LoggingMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Loading)).BeginInit();
            this.SuspendLayout();
            // 
            // LoggingMenu
            // 
            this.LoggingMenu.Controls.Add(this.labelLoggedIn);
            this.LoggingMenu.Controls.Add(this.groupBox1);
            this.LoggingMenu.Controls.Add(this.panel2);
            this.LoggingMenu.Location = new System.Drawing.Point(12, 12);
            this.LoggingMenu.Name = "LoggingMenu";
            this.LoggingMenu.Size = new System.Drawing.Size(233, 259);
            this.LoggingMenu.TabIndex = 34;
            this.LoggingMenu.TabStop = false;
            this.LoggingMenu.Text = "Logging Menu";
            this.LoggingMenu.Enter += new System.EventHandler(this.LoggingMenu_Enter);
            // 
            // labelLoggedIn
            // 
            this.labelLoggedIn.AutoSize = true;
            this.labelLoggedIn.Font = new System.Drawing.Font("Mistral", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLoggedIn.ForeColor = System.Drawing.Color.LimeGreen;
            this.labelLoggedIn.Location = new System.Drawing.Point(52, 228);
            this.labelLoggedIn.Name = "labelLoggedIn";
            this.labelLoggedIn.Size = new System.Drawing.Size(132, 19);
            this.labelLoggedIn.TabIndex = 34;
            this.labelLoggedIn.Text = "YOU ARE LOGGED IN";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPathSelector);
            this.groupBox1.Controls.Add(this.tbPath);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(12, 171);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 49);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Additional options";
            // 
            // btnPathSelector
            // 
            this.btnPathSelector.Location = new System.Drawing.Point(152, 19);
            this.btnPathSelector.Name = "btnPathSelector";
            this.btnPathSelector.Size = new System.Drawing.Size(42, 21);
            this.btnPathSelector.TabIndex = 28;
            this.btnPathSelector.Text = "path";
            this.btnPathSelector.UseVisualStyleBackColor = true;
            this.btnPathSelector.Click += new System.EventHandler(this.btnPathSelector_Click);
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(6, 19);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(143, 20);
            this.tbPath.TabIndex = 26;
            this.tbPath.Text = "Select path";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Loading);
            this.panel2.Controls.Add(this.btnLogOut);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btnLogIn);
            this.panel2.Controls.Add(this.textBoxPassword);
            this.panel2.Controls.Add(this.textBoxEmail);
            this.panel2.Location = new System.Drawing.Point(12, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(203, 149);
            this.panel2.TabIndex = 33;
            // 
            // Loading
            // 
            this.Loading.BackColor = System.Drawing.Color.Transparent;
            this.Loading.Location = new System.Drawing.Point(58, 35);
            this.Loading.Name = "Loading";
            this.Loading.Size = new System.Drawing.Size(91, 74);
            this.Loading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Loading.TabIndex = 14;
            this.Loading.TabStop = false;
            // 
            // btnLogOut
            // 
            this.btnLogOut.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnLogOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnLogOut.ForeColor = System.Drawing.Color.Firebrick;
            this.btnLogOut.Location = new System.Drawing.Point(106, 117);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(75, 29);
            this.btnLogOut.TabIndex = 35;
            this.btnLogOut.Text = "Log out";
            this.btnLogOut.UseVisualStyleBackColor = false;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, -3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Logging in";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(22, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 16);
            this.label1.TabIndex = 3;
            // 
            // btnLogIn
            // 
            this.btnLogIn.BackColor = System.Drawing.SystemColors.Control;
            this.btnLogIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnLogIn.ForeColor = System.Drawing.Color.LimeGreen;
            this.btnLogIn.Location = new System.Drawing.Point(25, 116);
            this.btnLogIn.Name = "btnLogIn";
            this.btnLogIn.Size = new System.Drawing.Size(75, 30);
            this.btnLogIn.TabIndex = 2;
            this.btnLogIn.Text = "Log in";
            this.btnLogIn.UseVisualStyleBackColor = false;
            this.btnLogIn.Click += new System.EventHandler(this.OnBtnLogInClick);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(25, 80);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(156, 20);
            this.textBoxPassword.TabIndex = 1;
            this.textBoxPassword.Text = "Password";
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.CausesValidation = false;
            this.textBoxEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxEmail.Location = new System.Drawing.Point(25, 35);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(156, 20);
            this.textBoxEmail.TabIndex = 0;
            this.textBoxEmail.Text = "Username";
            // 
            // LoggingInForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 282);
            this.Controls.Add(this.LoggingMenu);
            this.Name = "LoggingInForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.LoggingInForm_Load);
            this.LoggingMenu.ResumeLayout(false);
            this.LoggingMenu.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Loading)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox LoggingMenu;
        private System.Windows.Forms.Label labelLoggedIn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnPathSelector;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.PictureBox Loading;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLogIn;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxEmail;
    }
}

