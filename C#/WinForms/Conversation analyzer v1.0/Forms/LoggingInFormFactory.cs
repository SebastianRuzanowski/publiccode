﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnalyzerView.Interfaces;
using LoggingInModel;

namespace AnalyzerView
{
    public static class LoggingInFormFactory
    {
        private static ILoggingIn _loggingInForm = null;

        public static ILoggingIn Create()
        {
            if (_loggingInForm != null)
                return _loggingInForm;

            return new LoggingInForm();
        }

        public static void SetInstance(ILoggingIn loggingIn)
        {
            _loggingInForm = loggingIn;
        }
    }
}
