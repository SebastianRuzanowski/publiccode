﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace AnalyzerView
{
    partial class AnalyzerMainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Browser Infomation", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Conversation Information", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("Messages Information", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "Address",
            "Value"}, -1);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "Logged as",
            "Value"}, -1);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "Messages all",
            "Value"}, -1);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {
            "Messages loaded",
            "Value"}, -1);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "Messages left",
            "Value"}, -1);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem(new string[] {
            "Member1 of conversation",
            "Value"}, -1);
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem(new string[] {
            "Member2 of conversation",
            "Value"}, -1);
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem(new string[] {
            "Messages loaded [%]",
            "Value"}, -1);
            this.ButtonUpdate = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lvInformations = new System.Windows.Forms.ListView();
            this.Property = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Value = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label7 = new System.Windows.Forms.Label();
            this.btnLogInFormAdd = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelLoggedIn = new System.Windows.Forms.Label();
            this.btnGetFriends = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonUpdate
            // 
            this.ButtonUpdate.Location = new System.Drawing.Point(170, 25);
            this.ButtonUpdate.Name = "ButtonUpdate";
            this.ButtonUpdate.Size = new System.Drawing.Size(91, 51);
            this.ButtonUpdate.TabIndex = 8;
            this.ButtonUpdate.Text = "Update";
            this.ButtonUpdate.UseVisualStyleBackColor = true;
            this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(170, 88);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(57, 44);
            this.button2.TabIndex = 14;
            this.button2.Text = "Start Parsing";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(262, 109);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(187, 23);
            this.progressBar1.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(259, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Załadowane wiadomości";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(600, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 19;
            // 
            // lvInformations
            // 
            this.lvInformations.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Property,
            this.Value});
            listViewGroup1.Header = "Browser Infomation";
            listViewGroup1.Name = "Browser Infomation";
            listViewGroup2.Header = "Conversation Information";
            listViewGroup2.Name = "Conversation Information";
            listViewGroup3.Header = "Messages Information";
            listViewGroup3.Name = "Messages Information";
            this.lvInformations.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2,
            listViewGroup3});
            listViewItem1.Group = listViewGroup1;
            listViewItem1.UseItemStyleForSubItems = false;
            listViewItem2.Group = listViewGroup1;
            listViewItem3.Group = listViewGroup3;
            listViewItem3.IndentCount = 1;
            listViewItem4.Group = listViewGroup3;
            listViewItem5.Group = listViewGroup3;
            listViewItem6.Group = listViewGroup2;
            listViewItem7.Group = listViewGroup2;
            listViewItem8.Group = listViewGroup3;
            this.lvInformations.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8});
            this.lvInformations.Location = new System.Drawing.Point(12, 138);
            this.lvInformations.Name = "lvInformations";
            this.lvInformations.Size = new System.Drawing.Size(244, 530);
            this.lvInformations.TabIndex = 23;
            this.lvInformations.UseCompatibleStateImageBehavior = false;
            this.lvInformations.View = System.Windows.Forms.View.Tile;
            // 
            // Property
            // 
            this.Property.Text = "Property";
            this.Property.Width = 162;
            // 
            // Value
            // 
            this.Value.Text = "Value";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(227, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 31;
            // 
            // btnLogInFormAdd
            // 
            this.btnLogInFormAdd.BackColor = System.Drawing.SystemColors.Control;
            this.btnLogInFormAdd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLogInFormAdd.Location = new System.Drawing.Point(6, 19);
            this.btnLogInFormAdd.Name = "btnLogInFormAdd";
            this.btnLogInFormAdd.Size = new System.Drawing.Size(122, 38);
            this.btnLogInFormAdd.TabIndex = 32;
            this.btnLogInFormAdd.Text = "Log In";
            this.btnLogInFormAdd.UseVisualStyleBackColor = false;
            this.btnLogInFormAdd.Click += new System.EventHandler(this.btnLogInFormAdd_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelLoggedIn);
            this.groupBox1.Controls.Add(this.btnLogInFormAdd);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(152, 91);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Logging Menu";
            // 
            // labelLoggedIn
            // 
            this.labelLoggedIn.AutoSize = true;
            this.labelLoggedIn.Font = new System.Drawing.Font("Mistral", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLoggedIn.ForeColor = System.Drawing.Color.LimeGreen;
            this.labelLoggedIn.Location = new System.Drawing.Point(6, 69);
            this.labelLoggedIn.Name = "labelLoggedIn";
            this.labelLoggedIn.Size = new System.Drawing.Size(132, 19);
            this.labelLoggedIn.TabIndex = 35;
            this.labelLoggedIn.Text = "YOU ARE LOGGED IN";
            // 
            // btnGetFriends
            // 
            this.btnGetFriends.Location = new System.Drawing.Point(12, 109);
            this.btnGetFriends.Name = "btnGetFriends";
            this.btnGetFriends.Size = new System.Drawing.Size(77, 25);
            this.btnGetFriends.TabIndex = 35;
            this.btnGetFriends.Text = "GetFriends";
            this.btnGetFriends.UseVisualStyleBackColor = true;
            this.btnGetFriends.Click += new System.EventHandler(this.btnGetFriends_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(262, 138);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(361, 530);
            this.dataGridView1.TabIndex = 36;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(871, 25);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(709, 697);
            this.webBrowser1.TabIndex = 37;
            // 
            // AnalyzerMainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1470, 749);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnGetFriends);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lvInformations);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.ButtonUpdate);
            this.Name = "AnalyzerMainView";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Button ButtonUpdate;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ColumnHeader Property;
        private System.Windows.Forms.ColumnHeader Value;
        public System.Windows.Forms.ListView lvInformations;
        private System.Windows.Forms.Label label7;
        public Button btnLogInFormAdd;
        private GroupBox groupBox1;
        private Label labelLoggedIn;
        private Button btnGetFriends;
        public DataGridView dataGridView1;
        private WebBrowser webBrowser1;
    }
}

