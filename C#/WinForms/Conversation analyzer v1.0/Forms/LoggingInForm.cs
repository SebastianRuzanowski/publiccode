﻿using System;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Linq;
using AnalyzerView;
using AnalyzerView.Interfaces;

namespace AnalyzerView
{
    public partial class LoggingInForm : Form, ILoggingIn
    {
        
        public string Password
        {
            set { textBoxPassword.Text = value; }
        }

        public string Email
        {
            set { textBoxEmail.Text = value; }
        }

        public void LoggedInLogic(bool loggedIn)
        {

            if (loggedIn)
            {
                label1.Text = "You are logged in.";
                btnLogIn.Enabled = false;
                textBoxEmail.Enabled = false;
                textBoxPassword.Enabled = false;
                labelLoggedIn.Text = "YOU ARE LOGGED IN.";
                labelLoggedIn.ForeColor = Color.LimeGreen;
                btnLogIn.Enabled = false;
                textBoxEmail.Enabled = false;
                textBoxPassword.Enabled = false;
                btnLogOut.Enabled = true;



            }
            else
            {
                label1.Text = "You are not logged in.";
                btnLogIn.Enabled = true;
                textBoxEmail.Enabled = true;
                textBoxPassword.Enabled = true;
                labelLoggedIn.Text = "PLEASE, LOG IN.";
                labelLoggedIn.ForeColor = Color.Crimson;
                btnLogIn.Enabled = true;
                textBoxEmail.Enabled = true;
                textBoxPassword.Enabled = true;

            }

        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            //view.LogOut();
        }
        public LoggingInForm()
        {
            InitializeComponent();
        }

        private void btnPathSelector_Click(object sender, EventArgs e)
        {
           
        }

        private void OnBtnLogInClick(object sender, EventArgs e)
        {
            Mediator.Mediator.PassToMainViewLogins(textBoxEmail.Text,textBoxPassword.Text);
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            this.Hide();


            using (var context = new AnalyzerEntities1())
            {
                // Query for all blogs with names starting with B 
                //var blogs = from b in context.Person
                //            where b.name.StartsWith("B")
                //            select b;

                // Query for the Blog named ADO.NET Blog 
                //var blog = context.Blogs
                //                .Where(b => b.Name == "ADO.NET Blog")
                //                .FirstOrDefault();
            }
        }

    }
}
