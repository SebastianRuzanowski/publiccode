﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using ConversationParser;
using AnalyzerView;
using AnalyzerView.Interfaces;
using LoggingInModel;
using Timer = System.Windows.Forms.Timer;

namespace AnalyzerView
{
    public partial class AnalyzerMainView : Form, IAnalyzerView
    {
        #region Fields


        private readonly ILoggingInModel _logger;
        private readonly IConversationParser _parser;
        private LoggingInForm loggingInForm  = new LoggingInForm();
        private readonly IFriends _friends;

        public Timer timer = new System.Windows.Forms.Timer();

        private bool isLoggedIn;
        private int heightOfDocument;
        private int indexOfLoop = 0;
        private int numberOfFirstLoadMessages;
        private int numberOfActualLoadedMessages;
        private List<string> informationForViewFromWeb;
        private bool firstLoadNumber = true;
        #endregion

        #region Properties
        public string Link {  set { lvInformations.Items[0].SubItems[1].Text = value; } }

        public bool SetStateOfBtnNewFormCreator
        {
            set
            {
                if (value)
                {
                    btnLogInFormAdd.Enabled = false;
                    labelLoggedIn.Text = "YOU ARE LOGGED IN.";
                    labelLoggedIn.ForeColor = Color.LimeGreen;
                }
                else
                {
                    btnLogInFormAdd.Enabled = true;
                    labelLoggedIn.Text = "PLEASE, LOG IN.";
                    labelLoggedIn.ForeColor = Color.Crimson;
                }

            }
        }
        #endregion

        #region C'tor
        public AnalyzerMainView()
        {
            InitializeComponent();
            _logger = LoggingInModelFactory.Create();
            _parser = ConversationParserFactory.Create();
            _friends = new Friends();
     

            webBrowser1.DocumentCompleted += NavigateWebBrowserOnFacebook;
            webBrowser1.DocumentCompleted += Web_DocumentCompleted1;
            webBrowser1.Navigate(@"https://www.facebook.com/");
            DatabaseLoad();
        }

        private void DatabaseLoad()
        {


                AnalyzerEntities1 db = new AnalyzerEntities1();
            dataGridView1.DataSource = (from p in db.Person
                                        select p).ToList();

            //     dataGridView1.DataSource = (from p in db.Conversation select p.id).ToList();

        }

        #endregion

        #region On Click Events
        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            string personForConversation = @"https://www.facebook.com/messages/mateusz.krysztopowicz";

            webBrowser1.Navigate(personForConversation);
            timer.Interval = 1000;
            timer.Enabled = true;
            webBrowser1.DocumentCompleted += Web_DocumentCompleted;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _parser.Parse(webBrowser1);
            timer?.Stop();
        }
        
        private void btnLogInFormAdd_Click(object sender, EventArgs e)
        {
            loggingInForm.Show();
       
        }

        private void btnGetFriends_Click(object sender, EventArgs e)
        {
            _friends.GetFriends(webBrowser1);
        }

        #endregion

        #region Methods
        public bool IsLoggedIn
        {
            get { return isLoggedIn; }
            set
            {
                isLoggedIn = value;
                loggingInForm.LoggedInLogic(isLoggedIn);
                SetStateOfBtnNewFormCreator = value;
            }
        }

        public string PathOfLogins { get; set; }
      
        private void Web_DocumentCompleted1(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            IsLoggedIn = _logger.CheckIfLoggedIn(webBrowser1);
            webBrowser1.DocumentCompleted -= Web_DocumentCompleted1;
        }

        private void NavigateWebBrowserOnFacebook(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            IsLoggedIn = _logger.CheckIfLoggedIn(webBrowser1);
            var webBrowser = sender as WebBrowser;
            webBrowser.DocumentCompleted -= NavigateWebBrowserOnFacebook;
            Link = webBrowser.Url.ToString();
            LogIn(String.Empty, String.Empty);
        }

        public void LogIn(string email, string password)
        {
            if (PathOfLogins != String.Empty && IsLoggedIn == false && email != String.Empty && password != String.Empty && email != "Username")
            {
                var passwordsToLogIn = _logger.GetThePasswordsFromFile(PathOfLogins);

                if (passwordsToLogIn != null)
                {
                    loggingInForm.Email = passwordsToLogIn[0];
                    loggingInForm.Password = passwordsToLogIn[1];

                    webBrowser1.Document.GetElementById("email").SetAttribute("value", passwordsToLogIn[0]);
                    webBrowser1.Document.GetElementById("pass").SetAttribute("value", passwordsToLogIn[1]);

                    _logger.ClickButtonLoggingConfirm(webBrowser1);

                }
            }
            else if (email != String.Empty && password != String.Empty && email != "Username")
            {
                webBrowser1.Document.GetElementById("email").SetAttribute("value", email);
                webBrowser1.Document.GetElementById("pass").SetAttribute("value", password);

                _logger.ClickButtonLoggingConfirm(webBrowser1);
            }
        }

        public void LogOut()
        {

            //HtmlElement button1 =
            //    web.Document.GetElementById("logoutMenu");


            //if (button1 != null)
            //    button1.InvokeMember("click");
            //Task.Delay(500).ContinueWith(t => Logout2());

            //      IsLoggedIn = facebook.CheckIfLoggedIn(web); // TODO: LOGOUT TO THE END

            //try
            //{
            //    HtmlElement lit = null;
            //    webBrowser1.Invoke(new Action(() =>
            //    {
            //        lit =
            //             webBrowser1.Document.GetElementsByTagName("form")
            //                .Cast<HtmlElement>()
            //                .Where(x => x.InnerText == "Wyloguj się").ToList().FirstOrDefault();

            //    }));

            //    lit.InvokeMember("click"); //TODO: NULLREFERENCE EXCEPTIO


            //    //foreach (HtmlElement button in Elements)
            //    //{
            //    //    if (button.GetAttribute("className") == "_54ni navSubmenu __MenuItem" && button.InnerText == "Wyloguj się")
            //    //    {

            //    //    }
            //    //}
            //}
            //catch (Exception)
            //{

            //}
        }

        private void Web_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            timer.Interval = 1000;
            timer.Enabled = true;
            timer.Start();
            timer.Tick += ScrollConversation;

            webBrowser1.DocumentCompleted -= Web_DocumentCompleted;
        }

        private void ScrollConversation(object sender, EventArgs e)
        {
            try
            {

                if (webBrowser1.Document != null)
                {


                    var firstOrDefault = webBrowser1.Document.GetElementsByTagName("div")
                        .Cast<HtmlElement>()
                        .FirstOrDefault(m => m.GetAttribute("className") == "uiScrollableAreaShadow");

                    webBrowser1.Document.GetElementsByTagName("div")
                        .Cast<HtmlElement>()
                        .FirstOrDefault(m => m.GetAttribute("className") == "uiScrollableAreaShadow")?.ScrollIntoView(true);

                    if (firstOrDefault != null)
                        heightOfDocument = firstOrDefault.ClientRectangle.Height;


                    HtmlElementCollection classButton = webBrowser1.Document.All;
                    foreach (HtmlElement element in classButton)
                    {
                        if (element.GetAttribute("className") == "pam uiBoxLightblue uiMorePagerPrimary" &&
                            element.InnerText.StartsWith("Załaduj starsze wiadomości ("))
                        {
                            element.InvokeMember("click");

                            break;
                        }

                    }

                    #region if it's not acrueing

                    if (indexOfLoop % 20 == 0)
                    {
                        var isEndOfConversation = webBrowser1.Document.GetElementsByTagName("li")
                            .Cast<HtmlElement>()
                            .FirstOrDefault(m => m.GetAttribute("className") == "timestamp");

                        if (isEndOfConversation != null &&
                            isEndOfConversation.InnerText.StartsWith("Konwersacja rozpoczęła się"))
                        {
                            webBrowser1.DocumentCompleted -= ScrollConversation;
                            timer.Stop();
                        }
                    }
                }

                #endregion

                #region update informations

                if (indexOfLoop % 5 == 0 || indexOfLoop <= 1)
                {
                    var actualNumber = _parser.GetMessageLeft(webBrowser1);

                    informationForViewFromWeb = new List<string>()
                    {
                        "wysokość",
                        heightOfDocument.ToString(),
                        String.Empty,
                        "Zostało wiadomości:",
                        actualNumber
                    };

                    if (Convert.ToInt32(actualNumber) > 0)
                    {
                        if (firstLoadNumber)
                        {
                            firstLoadNumber = false;
                            numberOfFirstLoadMessages = Convert.ToInt32(actualNumber);
                            progressBar1.Maximum = numberOfFirstLoadMessages;
                        }
                        else if (Convert.ToInt32(actualNumber) > 0)
                        {
                            informationForViewFromWeb.Add("");
                            informationForViewFromWeb.Add("Załadowanych wiadomości: ");
                            numberOfActualLoadedMessages =
                                _parser.HowManyMessagesWereLoadedAlready(numberOfFirstLoadMessages,
                                    Convert.ToInt32(actualNumber));
                            informationForViewFromWeb.Add(numberOfActualLoadedMessages.ToString());
                        }

                        informationForViewFromWeb.Add("");
                        informationForViewFromWeb.Add("Liczba odświeżeń timera");
                        informationForViewFromWeb.Add(indexOfLoop.ToString());

                        progressBar1.Value = numberOfActualLoadedMessages;
                        label3.Text = (Convert.ToDouble(progressBar1.Value) / Convert.ToDouble(progressBar1.Maximum) * 100).ToString("##.00") + " %";
                    }
                }
            }
            catch
            {
                // ignored
            }

            #endregion

            indexOfLoop++;
        }

        public void GetFriends()
        {
            _friends.GetFriends(webBrowser1);

        }

#endregion
    }
}