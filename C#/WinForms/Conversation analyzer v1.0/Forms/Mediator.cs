﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnalyzerView;
using AnalyzerView.Interfaces;

namespace Mediator
{
    public static class Mediator 
    {
        
        public static void PassToMainViewLogins(string Email, string Password)
        {
            IAnalyzerView view = new AnalyzerMainView();
            view.LogIn(Email,Password);
        }

    }
}
