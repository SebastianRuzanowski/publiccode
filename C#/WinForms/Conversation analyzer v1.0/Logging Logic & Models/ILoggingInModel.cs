﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoggingInModel
{
    public interface ILoggingInModel
    {
        string[] GetThePasswordsFromFile(string path);
        void ClickButtonLoggingConfirm(WebBrowser web);
        bool CheckIfLoggedIn(WebBrowser web);

    }
}
