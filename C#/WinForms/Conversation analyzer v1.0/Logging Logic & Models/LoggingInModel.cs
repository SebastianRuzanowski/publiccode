﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoggingInModel
{
    class LoggingInModel : ILoggingInModel
    {

        public string[] GetThePasswordsFromFile(string path)
        {
            string[] loginLines = null;
            try
            {
                loginLines = System.IO.File.ReadAllLines(@path);
                loginLines[0] = loginLines[0].Replace("\r\n\t", string.Empty);
                loginLines[1] = loginLines[1].Replace("\r\n\t", string.Empty);


                // TODO: VALIDATION IF IT'S CORRECT
            }

            catch (Exception)
            {
            } // does nothing

            return loginLines;
           }

        public void ClickButtonLoggingConfirm(WebBrowser web)
        {

            HtmlElement button = web.Document.GetElementsByTagName("label")
                .Cast<HtmlElement>()
                .FirstOrDefault(m => m.GetAttribute("className") == "uiButton uiButtonConfirm");
            if (button != null)
                button.InvokeMember("click");

           
        }


        public bool CheckIfLoggedIn(WebBrowser web)
        {
            var document = new HtmlAgilityPack.HtmlDocument();
            try
            {
                document.LoadHtml(html: web.Document?.Body.OuterHtml);

                return document.DocumentNode.Descendants("a")
                    .Any(d =>
    d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("_2s25"));
                
            }
            catch
            {
                return false;
            }
        }
    }
}
