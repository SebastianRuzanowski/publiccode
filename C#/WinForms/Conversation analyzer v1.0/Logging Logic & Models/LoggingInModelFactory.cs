﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggingInModel;
namespace LoggingInModel
{

        public static class LoggingInModelFactory
        {
            private static ILoggingInModel _loggingInModel = null;

            public static ILoggingInModel Create()
            {
                if (_loggingInModel != null)
                    return _loggingInModel;

                return new LoggingInModel();
            }

            public static void SetInstance(ILoggingInModel loggingInModel)
            {
                _loggingInModel = loggingInModel;
            }
        }
}
