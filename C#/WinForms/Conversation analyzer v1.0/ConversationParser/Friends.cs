﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Text;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConversationParser
{
    public class Friends : IFriends
    {
        List<string> friendsNames = new List<string>();
        List<string> friendsID = new List<string>();
        List<int> heights = new List<int>();

        Timer timer1 = new Timer();
        
        private int index = 0;
        public List<string> GetFriends(WebBrowser web)
        {
            friendsNames.Clear();
            web.DocumentCompleted += Web_DocumentCompleted;
            web.Navigate(@"https://www.facebook.com/sebastian.ruzanowski/friends?source_ref=pb_friends_tl");
            timer1.Interval = 1000;
            timer1.Start();


            return friendsNames;
        }

        private void Web_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
           
            ((WebBrowser)sender).Document.Body.ScrollIntoView(false);
            timer1.Tick += delegate
            {
                ((WebBrowser)sender).Document.Body.ScrollIntoView(false);
                heights.Add(((WebBrowser)sender).Document.Body.ScrollRectangle.Height);
                if (index>10)
                if (heights[index] == heights[index-8])
                {
                   
                   if(timer1.Enabled)
                   FetchFriends((WebBrowser)sender);
                        timer1.Stop();

                   ((WebBrowser) sender).DocumentCompleted -= Web_DocumentCompleted;
                    return;
                }
                index++;
            };
        }

        private void FetchFriends(WebBrowser web)
        {
            var document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(web.Document.Body.OuterHtml);

            friendsNames = document.DocumentNode.Descendants()
                .Where(
                    x => x.Name == "div" && x.Attributes["class"] != null && x.Attributes["class"].Value == "fsl fwb fcb")
                .Select(x => x.ChildNodes[0].InnerText).ToList();

            var thrashID = web.Document.GetElementsByTagName("div")
                .Cast<HtmlElement>()
                .Where(x => x.GetAttribute("className") == "fsl fwb fcb")
                .Select(x => x.FirstChild.GetAttribute("data-gt")).ToList();
            foreach (var thrash in thrashID)
            {
                Match match = Regex.Match(thrash, "eng_tid\\\":\\\"([0-9]+)");
                var val = match.Value;
                string key = match.Groups[1].Value;
                friendsID.Add(key);
            }

            using (SqlConnection connection = new SqlConnection("Data Source=.;Initial Catalog=Analyzer;Integrated Security=True"))
            {
                
                for (int i = 0; i < friendsNames.Count; i++)
                {
                    
                    SqlCommand cmd = new SqlCommand("INSERT INTO Person VALUES (@Name, @link)", connection);

                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@name", friendsNames[i]);
                    cmd.Parameters.AddWithValue("@link", friendsID[i]);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
                
            }
        }

    }
}
    