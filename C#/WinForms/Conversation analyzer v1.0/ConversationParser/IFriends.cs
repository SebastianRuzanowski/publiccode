﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConversationParser
{
    public interface IFriends
    {
        List<string> GetFriends(WebBrowser web);
    }
}
