﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Messaging;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using ConversationParser;
using Timer = System.Threading.Timer;
using HtmlAgilityPack;
using HtmlDocument = System.Windows.Forms.HtmlDocument;
using Structures;
namespace ConversationParser
{
    public class ConversationParser : IConversationParser
    {
        public List<MessageStruct> Parse(WebBrowser web)
        {
            var listOfStructs = new List<MessageStruct>();
            #region ConversationID
            var conversationID = web.Url.ToString();
            var ConversationIDParsed = conversationID.Replace(@"https://www.facebook.com/messages/", String.Empty);
            ConversationIDParsed = ConversationIDParsed.Trim('#', '/');
#endregion

            #region listOfStructs
            var collectionOfMessages =
                web.Document
                    .GetElementsByTagName("div")
                    .Cast<HtmlElement>()
                    .Where(x => x.GetAttribute("className") == "clearfix _42ef")
                    .ToList();

            var listOfMessages = collectionOfMessages.Select(x => x.InnerText ).ToList();

            string[][] lines = new string[listOfMessages.Count][];
            for (int i = 0; i < listOfMessages.Count; i++)
            {
                lines[i] = listOfMessages[i].Split(new string[] {"\r\n", "\n"}, StringSplitOptions.None);
            }
            int index = 0;

            for (int i = 0; i < listOfMessages.Count; i++)
            {
                lines[i] = listOfMessages[i].Split(new string[] {"\r\n", "\n"}, StringSplitOptions.None);
                lines[i][1] = "";
            }
            string[][] purgedList = new string[listOfMessages.Count][];

            foreach (var line in lines)
            {

                purgedList[index] = line.Where(s => s != "").ToArray();
                index ++;
            }

            foreach (var t in purgedList)
            {
                for (int j = 2; j < t.Length; j++)
                {
                    var name = t[1];
                    var dateAfterCasting = GetTimeToParse(t[0]);
                    var singleMessage = t[j];
                    listOfStructs.Add(new MessageStruct(name, dateAfterCasting, singleMessage,""));
                }
            }
        #endregion


            // SerializeMessagesToXML(ref listOfStructs);
            ParseToDatabase(ref listOfStructs, ConversationIDParsed);
            return listOfStructs;
        }

        private void SerializeMessagesToXML(ref List<MessageStruct> listOfStructs )
        {
            XmlSerializer xmlSer = new XmlSerializer(typeof(List<MessageStruct>));
            TextWriter WriteFileStream = new StreamWriter(@"C:\Users\RS\Desktop\SERIALIZED.xml");
            xmlSer.Serialize(WriteFileStream, listOfStructs);
           

        }

        private void ParseToDatabase(ref List<MessageStruct> listOfStructs,string ConversationID)
        {
            //int ConversationID2 = Convert.ToInt32(ConversationID);
            //var db = new AnalyzerEntities();
            //int index = 0;
            //foreach (var messageStruct in listOfStructs)
            //{
            //    var PersonID = (from m in db.Message
            //        join p in db.Person on messageStruct.name equals p.name
            //        select p.PersonId).FirstOrDefault();

            //    var context = new AnalyzerEntities();

            //    var t = new Message() 
            //    {
            //        PersonID = PersonID,
            //        contentText =  messageStruct.message,
            //        contentHTML = messageStruct.messageHTML,
            //        DateTime = messageStruct.dateTime,
            //        ConversationID =  ConversationID2,
            //    };

            //    context.Message.Add(t);
            //    context.SaveChanges();

            //    index++;
       //     }

        }

        private DateTime GetTimeToParse(string dateAndTime)
        {

            if (dateAndTime.Length.Equals(15))
                dateAndTime = "0" + dateAndTime;
            else if (dateAndTime.Length.Equals(5))
            {
                var now = DateTime.Now.ToShortDateString();
                dateAndTime = DateTime.Now.ToString("dd.MM.yyyy") + " " + dateAndTime;
            }
            DateTime loadedDate = DateTime.ParseExact(dateAndTime, "dd.MM.yyyy HH:mm",
                System.Globalization.CultureInfo.InvariantCulture);

            return loadedDate;
        }

        public string GetMessageLeft(WebBrowser web)
        {
            var document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(web.Document.Body.OuterHtml);

            return document.DocumentNode.Descendants()
                .Where(
                    x => x.Name == "span" && x.Attributes["class"] != null && x.Attributes["class"].Value == "_7hy")
                .Select(x => x.ChildNodes[1].InnerText).ToList().First();
        }

        public int HowManyMessagesWereLoadedAlready(int firstValue, int secondValue) => firstValue - secondValue;
    }
}
