﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversationParser
{
    public static class ConversationParserFactory
    {
        private static IConversationParser _conversationParser = null;

        public static IConversationParser Create()
        {
            if (_conversationParser != null)
                return _conversationParser;

            return new ConversationParser();
        }

        public static void SetInstance(IConversationParser conversationParser)
        {
            _conversationParser = conversationParser;
        }
    }
}
