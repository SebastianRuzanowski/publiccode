﻿using System.Collections.Generic;
using System.Windows.Forms;
using Structures;

namespace ConversationParser
{
    public interface IConversationParser
    {
        List<MessageStruct> Parse(WebBrowser web);
        string GetMessageLeft(WebBrowser web);
        int HowManyMessagesWereLoadedAlready(int firstValue, int secondValue);
    }
}
