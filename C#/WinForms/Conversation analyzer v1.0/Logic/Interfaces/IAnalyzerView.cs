﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AnalyzerView;
namespace AnalyzerView.Interfaces
{
    public interface IAnalyzerView
    {
        string Link { set; }
        int SetMaximumValueProgressBar { set; }
        int SetActualValueProgressBar { set; }
        bool SetStateOfBtnNewFormCreator { set; }
    }
}
