﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalyzerView.Interfaces
{
    public interface ILoggingIn
    {
        string Password{set;}
        string Email {set;}
        void LoggedInLogic(bool loggedIn);
    }
}
