﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace Komunikator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static int i = 2;
        public MainWindow()
        {
            InitializeComponent();
        }

        //Zmiana aktywnej zakładnki z serwerami
        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(AddItem.IsSelected)
            {
                try
                {
                    TabItem tab = new TabItem();
                    tab.Header = "Serwer " + i;
                    i++;
                    Grid grid = new Grid();
                    tab.Content = grid;
                    grid.Children.Add(new Components.Server());

                    ((TabControl)sender).Items.Insert(((TabControl)sender).Items.Count - 1, tab);

                    ((TabControl)sender).SelectedItem = tab;
                }catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
    }
}
