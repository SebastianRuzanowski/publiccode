﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Komunikator
{
    class Client
    {

        public delegate void Receive(Byte[] data); 
        public delegate void Disconnect(); // Tworzone są eventy Receive i Disconnect. W momencie wywołania ich przez zewnętrzny parametr, zmieniane są wewnętrzne parametry, wywoływane dopowiednie metody itp.

        //Zdarzenia które są wywoływane podczas rozłączenia oraz odbiernaia danych
        public Receive receive;
        public Disconnect disconnect;
        TcpClient client = null;
        public Client(string host, Int32 port) // Podcas tworzenia Clienta HOST -> ip, PORT -> port przez który się łączymy
        {
            client = new TcpClient(host, port); // tworzymy następnie połączenie TCP o podanych parametrach
        }

        ~Client()
        {
            if(client != null) client.Close(); // destruktor, chociaż nie jest wymagany, bo w C# jest Garbage Collector
        }
        //Wysyłanie tekstu
        public void Send(string msg)
        {
            NetworkStream stream = client.GetStream();
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);
            stream.Write(data, 0, data.Length);
        }
        //Wysyłanie danych
        public void Send(byte[] msg)
        {
            NetworkStream stream = client.GetStream();
            stream.Write(msg, 0, msg.Length);
        }

        //Rozpoczęcie odbierania - osobny wątek
        public void StartReceiving()
        {
            var stream = client.GetStream();

            try
            {
                while (true)
                {
                    byte[] data = new byte[1024];
                    int size = stream.Read(data, 0, 1024);
                    if (size > 0)
                    {
                        receive(data);
                    }

                    Thread.Sleep(10);
                }
            }catch(Exception ex)
            {
                if (disconnect != null) disconnect();
            }
        }
    }
}
