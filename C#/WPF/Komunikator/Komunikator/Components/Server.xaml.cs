﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Security.Cryptography;
using System.IO;

namespace Komunikator.Components
{
    /// <summary>
    /// Interaction logic for Server.xaml
    /// </summary>
    public partial class Server : UserControl
    {
        enum Status {Login, Connected};
        Status status;
        Client client;
        byte[] key, iv;
        FileDownloader file;

        public Server()
        {
            InitializeComponent();
            status = Status.Login;

            login.connect = Connect;
        }

        //Łączenie się z hostem
        public void Connect(string host, int port, byte[] key, byte[] iv)
        {
            try
            {
                client = new Client("localhost", 13001);
                client.receive = this.Receive;
                client.disconnect = this.Disconnect;
                Thread thread = new Thread(() => client.StartReceiving());
                thread.Start();

                chat.Visibility = Visibility.Visible;
                chat.sendMessage = SendMessage;
                chat.sendfile = SendFile;
                login.Visibility = Visibility.Hidden;

                this.key = key;
                this.iv = iv;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
            
        //Wysyłanie wiadomosci tesktowej
        public void SendMessage(string msg)
        {                        
            var d = Security.EncryptStringToBytes_Aes(msg, key, iv);
            byte[] _d = new byte[d.Length + 8];
            BitConverter.GetBytes(1).CopyTo(_d, 0);
            BitConverter.GetBytes(d.Length).CopyTo(_d, 4);
            Buffer.BlockCopy(d, 0, _d, 8, d.Length);

            client.Send(_d);
           // string text = Security.DecryptStringFromBytes_Aes(d, key, iv);
        }

        //Wysyłanie pliku
        static int SIZE_PACKAGE = 512;
        public void SendFile(string name, string path)
        {
            //Wyświetlanie progressbara podczas wysyłania
            chat.Dispatcher.BeginInvoke((Action)(() =>
            {
                chat.sending.Visibility = Visibility.Visible;
            }));

            //Podzielenie pliku na paczki oraz nagłówek i pokolei wysyłanie wszystkich
            var data = File.ReadAllBytes(path);
            byte[] namebyte = Encoding.ASCII.GetBytes(name);
            byte[] begin = new byte[4+4 + 4 + namebyte.Length];

            BitConverter.GetBytes(2).CopyTo(begin, 0);
            BitConverter.GetBytes(data.Length).CopyTo(begin, 4);
            BitConverter.GetBytes(namebyte.Length).CopyTo(begin, 8);
            Buffer.BlockCopy(namebyte, 0, begin, 12, namebyte.Length);

            client.Send(begin);

            int packages = data.Length / SIZE_PACKAGE + 1;
            for (int i = 0; i < packages; i++)
            {
                int size = i == packages - 1 ? data.Length % SIZE_PACKAGE : SIZE_PACKAGE;
                byte[] _data = new byte[4 + 4 + 4 + size];
                BitConverter.GetBytes(3).CopyTo(_data, 0);
                BitConverter.GetBytes(i*SIZE_PACKAGE).CopyTo(_data, 4);
                BitConverter.GetBytes(size).CopyTo(_data, 8);
                Buffer.BlockCopy(data, i*SIZE_PACKAGE, _data, 12, size);
                client.Send(_data);
                Thread.Sleep(20);
            }

            //Gdy ukończone jest wysyłanie przywracamy pole do wpisywania tekstu
            chat.Dispatcher.BeginInvoke((Action)(() =>
            {
                chat.sending.Visibility = Visibility.Hidden;
            }));
        }

        //Odbieranie wiadomości
        public void Receive(Byte[] data)
        {
            //Sprawdzanie typu wiadomości
            int idMessage = BitConverter.ToInt32(data, 0);

            switch(idMessage)
            {
                case 1://Przyszla wiadomosc 
                    {
                        int size = BitConverter.ToInt32(data, 4);
                        byte[] _d = new byte[size];
                        Buffer.BlockCopy(data, 8, _d, 0, size);

                        string text = Security.DecryptStringFromBytes_Aes(_d, key, iv);
                        chat.Dispatcher.BeginInvoke((Action)(() => chat.AddMessage(text, false)));
                    }
                    break;

                case 2://Naglowek pliku
                    {;
                        int filesize = BitConverter.ToInt32(data, 4);
                        int sizeName = BitConverter.ToInt32(data, 8);
                        byte[] namebyte = new byte[sizeName];
                        Buffer.BlockCopy(data, 12, namebyte, 0, sizeName);
                        string name = Encoding.ASCII.GetString(namebyte);
                        file = new FileDownloader(name, filesize);
                    }
                    break;

                case 3://Dane pliku
                    {
                        int offset = BitConverter.ToInt32(data, 4);
                        int sizePackage = BitConverter.ToInt32(data, 8);
                        byte[] filedata = new byte[sizePackage];
                        Buffer.BlockCopy(data, 12, filedata, 0, sizePackage);
                        if(file.AddData(filedata, offset))
                        {
                            chat.Dispatcher.BeginInvoke((Action)(() => chat.AddFile(file.GetPath(), false)));
                        }
                    }
                    break;
            }
           
        }
        
        //Rozłączenie klienta
        public void Disconnect()
        {
            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                chat.Visibility = Visibility.Hidden;
                login.Visibility = Visibility.Visible;
            }));
        }        
    }
}
