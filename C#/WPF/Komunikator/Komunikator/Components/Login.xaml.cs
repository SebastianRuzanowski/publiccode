﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Komunikator.Components
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : UserControl
    {
        public delegate void Connect(string host, int port, byte[] key, byte[] iv);
        public Connect connect;

        private byte[] key, iv;

        public Login()
        {
            InitializeComponent();
        }

        //Nawiązywanie połączenia z serwerem
        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (connect != null) try
                {
                    var data = File.ReadAllBytes(textBox_Key.Text);
                    int sizeKey = BitConverter.ToInt32(data, 0);
                    key = new byte[sizeKey];
                    Buffer.BlockCopy(data, 4, key, 0, sizeKey);
                    int sizeIV = BitConverter.ToInt32(data, sizeKey + 4);
                    iv = new byte[sizeIV];
                    Buffer.BlockCopy(data, 8 + sizeKey, iv, 0, sizeIV);

                    connect(textBox_host.Text, Convert.ToInt32(textBox_port.Text), key, iv);
                } catch (FormatException fe)
                {
                    MessageBox.Show("Popraw dane");
                }
        }

        //Wtbuerabue pliku z kluczem szyfrującym
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Security key|*.key";
            openFileDialog1.Title = "Wybierz plik zabezpieczenia";
            
            if (openFileDialog1.ShowDialog() == true)
            {
                textBox_Key.Text = openFileDialog1.InitialDirectory + openFileDialog1.FileName;
            }
        }
    }
}
