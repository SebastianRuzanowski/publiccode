﻿using System;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace Komunikator.Components
{
    /// <summary>
    /// Interaction logic for Chat.xaml
    /// </summary>
    public partial class Chat : UserControl
    {
        private Brush colorMe = Brushes.Aqua;
        private Brush colorOther = Brushes.LightSkyBlue;

        public delegate void SendMessage(string msg);
        public SendMessage sendMessage;

        public delegate void SendFile(string name, string path);
        public SendFile sendfile;

        public Chat()
        {
            InitializeComponent();
        }

        //Dodanie wiadomości w wyświetlanym oknie czatu
        public void AddMessage(string msg, bool me = true)
        {
            //Wychwytywanie linków z treści wiadomości
            List<Object> items = new List<Object>();

            var linkParser = new Regex(@"\b(?:https?://|www\.)\S+\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var rawString = msg;
            int prev = 0;
            foreach (Match m in linkParser.Matches(rawString))
            {
                string tmpStr = rawString.Substring(prev, m.Index - prev);
                items.Add(tmpStr);
                prev = m.Index + m.Length;

                string href = m.Value;
                if (m.Value.Substring(0, 3) == "www") href = "http://" + href;

                Hyperlink hl = new Hyperlink()
                {
                    NavigateUri = new Uri(href)
                };
                hl.Inlines.Add(m.Value);
                hl.RequestNavigate += Hl_RequestNavigate;
                items.Add(hl);
            }
            string t = rawString.Substring(prev, rawString.Length - prev);
            items.Add(t);

            //Tworzenie wiadomosci do wyświetlenia
            TextBlock tb = new TextBlock();
            foreach(var item in items)
            {
                if(item as Hyperlink != null)
                    tb.Inlines.Add((Hyperlink)item);
                else if (item as string != null)
                    tb.Inlines.Add((string)item);
            }

            if(me)
                listBox.Items.Add(new ListBoxItem { Content = tb, Background = colorMe });
            else
                listBox.Items.Add(new ListBoxItem { Content = tb, Background = colorOther });

            listBox.SelectedIndex = listBox.Items.Count - 1;
            listBox.ScrollIntoView(listBox.SelectedItem);
        }

        //Dodanie pliku do okna czatu
        public void AddFile(string path, bool me = true)
        {
            Object content;

            string extension = System.IO.Path.GetExtension(path);
            switch(extension.ToLower())
            {
                case ".jpg":
                case ".png":
                case ".bmp":
                    Image img = new Image();
                    img.Width = 400;
                    img.Source = new BitmapImage(new Uri(path));
                    
                    content = img;
                    break;
                default:
                    TextBlock tb = new TextBlock();

                    Hyperlink hl = new Hyperlink()
                    {
                        NavigateUri = new Uri(path)
                    };
                    hl.Inlines.Add(path);
                    hl.RequestNavigate += Hl_RequestNavigate;
                    tb.Inlines.Add(hl);
                    content = tb;
                    break;
            }



            if (me)
                listBox.Items.Add(new ListBoxItem { Content = content, Background = colorMe });
            else
                listBox.Items.Add(new ListBoxItem { Content = content, Background = colorOther });

            listBox.SelectedIndex = listBox.Items.Count - 1;
            listBox.ScrollIntoView(listBox.SelectedItem);
        }

        //Funkcja wywoływana podczas kkiknięcia w link
        private void Hl_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Uri.AbsoluteUri);
        }
        
        //Wysłanie widomości
        private void button_Click(object sender, RoutedEventArgs e)
        {
            SendText();
        }

        //Wysłanie tesktu
        public void SendText()
        {
            if (textBox.Text == "") return;

            AddMessage(textBox.Text);
            sendMessage(textBox.Text);
            textBox.Text = "";
        }

        //Przechwycenie wcisnięcia Entere aby wysłać wiadomość
        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SendText();
            }
        }

        //Wybieranie okna dla wysyłania pliku
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Title = "Wybierz plik";

            if (openFileDialog1.ShowDialog() == true)
            {
                if (sendfile != null)
                {
                    Thread thread = new Thread(() => {
                        sendfile(openFileDialog1.SafeFileName, openFileDialog1.FileName);

                        this.Dispatcher.BeginInvoke((Action)(() => AddFile(openFileDialog1.FileName)));
                    });
                    thread.Start();
                }
            }
        }

        //Usunięcie "koloru" zacznaonego pola w czacie
        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            listBox.SelectedIndex = -1;
        }
    }
}
