﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Komunikator
{
    //Klasa slużąca pobieraniu plików do komunikatora
    class FileDownloader
    {
        private byte[] data;
        private int uploadSize;
        private string name;
        private Int32 offset = 0;
        private string path;

        //Ustawienie folderu do pobierania plikow
        public FileDownloader(string filename, int size)
        {
            path = "tmp";
            name = filename;
            data = new byte[size];
            uploadSize = 0;
        }

        //Dodanie kolejnej porcji danych do pobieranego pliku
        public bool AddData(byte[] dataFile, int offset)
        {
            Buffer.BlockCopy(dataFile, 0, data, offset, dataFile.Length);
            uploadSize += dataFile.Length;
            if (data.Length == uploadSize)
            {
                Finish();
                return true;
            }
            return false;
        }

        //Zwraca pełną ścieżkę do pliku
        public string GetPath()
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "/" + path + "/" + name;
        }

        //Funkcja wywoływana jest podczas zakończeniea pobierania
        public void Finish()
        {
            try
            {
                //Zapisanie pobranych danych do pliku
                FileStream file;

                Directory.CreateDirectory(path);
                file = new FileStream(path + "/" + name, FileMode.Append, FileAccess.Write);

                file.Write(data, 0, data.Length); 
                file.Close();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
