﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Security.Cryptography;
using System.IO;

namespace Serwer
{
    class Server
    {
        TcpListener server;
        List<Client> clients = new List<Client>();
        bool encryption = true;

        //Inicjalizacja serwera na domyślnym porcie 13001
        public Server(Int32 port = 13001)
        {
            IPAddress localAddr = IPAddress.Parse("127.0.0.1");
            server = new TcpListener(localAddr, port);

            if (encryption)
            {
                using (Aes aesAlg = Aes.Create())
                {
                    int sizekey = aesAlg.Key.Length;
                    int sizeiv = aesAlg.IV.Length;
                    byte[] filedata = new byte[sizekey + sizeiv + 8];

                    BitConverter.GetBytes(sizekey).CopyTo(filedata, 0);
                    aesAlg.Key.CopyTo(filedata, 4);
                    BitConverter.GetBytes(sizeiv).CopyTo(filedata, 4 + sizekey);
                    aesAlg.IV.CopyTo(filedata, 8 + sizekey);

                    File.WriteAllBytes("server.key", filedata);
                }
            }
        }

        //Uruchominenie serwera
        public void Start()
        {
            server.Start();
            Console.WriteLine("Server start");

            while (true)
            {
                TcpClient client = server.AcceptTcpClient();
                CreateClient(client);
            }
        }

        //Stworzneie nowego połączonego klienta
        private void CreateClient(TcpClient client)
        {
            Console.WriteLine("Client conneted");
            Client newClient = new Client(client, this);
            Thread thread = new Thread(() => newClient.StartReceiving());
            thread.Start();
            clients.Add(newClient);
        }

        //Rozsyłanie widaomości dalej - dla wszystkich innych klientów
        public void  SendToOtherClients(Client sender, Byte[] data)
        {
            foreach(var client in clients)
            {
                if (client == sender) continue;
                client.Send(data);
            }
        }
    }
}
