﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Serwer
{
    class Client
    {
        private TcpClient client;
        private Server server;

        //Stworzneie nowego klienta
        public Client(TcpClient client, Server server)
        {
            this.client = client;
            this.server = server;
        }

        //Nasłuchiwanie pakietów dla danego klienta - nowy wątek
        public void StartReceiving()
        {
            var stream = client.GetStream();

            while(true)
            {
                byte[] data = new byte[1024];
                int size = stream.Read(data, 0, 1024);
                if(size > 0)
                {
                    Receive(data);
                }

                Thread.Sleep(10);
            }
        }

        //Odebranie pakeitu
        public void Receive(Byte[] data)
        {
            //TODO: Obsługa danych
            var text = System.Text.Encoding.ASCII.GetString(data);
            text = text.Substring(0, text.IndexOf('\0'));
            //System.Console.WriteLine(text);

            server.SendToOtherClients(this, data);
        }

        //Wysłanie pakietuy danych
        public void Send(Byte[] data)
        {
            NetworkStream stream = client.GetStream();
            stream.Write(data, 0, data.Length);
        }
    }
}
