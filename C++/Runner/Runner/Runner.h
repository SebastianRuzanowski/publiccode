#pragma once
#include <string>
using namespace std;
class Runner
{
	int x, y;
	char kodZnaku;
	string color;
public:
	void gotoxy();
	void napiszZnak(char c);
	void setX(int x);
	void setY(int y);
	int getX();
	int getY();
	Runner(int x, int y, char kodZnaku,string color);
};

