#include "Runner.h"
#include <iostream>
#include <Windows.h>
#include <conio.h>
#include "colors.h"
#include <string>
using namespace std;
void Runner::gotoxy()
{
	COORD c;
	c.X = x-1;
	c.Y = y-1;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
}
void Runner::napiszZnak(char c='*')
{

	_putch(c);
}
void Runner::setX(int x)
{
	this->x = x;
}
void Runner::setY(int y)
{
	this->y = y;
}
int Runner::getX()
{
	return x;
}
int Runner::getY()
{
	return y;
}
Runner::Runner(int x, int y, char kodZnaku,string color="blue")
		:x(x),
		y(y),
		kodZnaku(kodZnaku),
	    color(color)
{
	gotoxy();
	napiszZnak();
}
