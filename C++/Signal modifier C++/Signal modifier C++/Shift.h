#pragma once
#include "Signal.h"
#include "BasicBlock.h"
class Shift : public BasicBlock
{
public:
	void process(Signal &signal);
};

