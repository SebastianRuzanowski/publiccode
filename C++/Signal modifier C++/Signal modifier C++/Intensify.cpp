#include "stdafx.h"
#include "Intensify.h"

void Intensify::process(Signal &signal) {
	float value;
	for (int j = 0; j < 50; j++) {
		value = signal[j] * signal[j];
		signal[j] = value;
	}
}