#pragma once
#include "Signal.h"
#include "BasicBlock.h"
class Amplify : public BasicBlock
{
public:
	void process(Signal &s);
};

