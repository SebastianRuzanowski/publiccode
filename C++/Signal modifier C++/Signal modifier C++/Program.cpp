#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <vector>
using namespace std;
class Signal
{
private:
	vector<float> data;
	float x;
	int signals[50];
public:
	void readData(Signal &s);
	friend ifstream &operator >> (ifstream &str, Signal &signal);
	friend ofstream &operator <<(ofstream &str, Signal &signal);
	float& operator[](int el);
	void writeData(Signal &s);
	void setD(float Data, int iterator);
	int& operator()(int el);
};
class BasicBlock
{
public:
	virtual void process(Signal &stream) = 0;
};
class Amplify : public BasicBlock
{
public:
	void process(Signal &s);
};
class Discretize : public BasicBlock
{
public:

	void process(Signal &s);
};
class Intensify : public BasicBlock
{
public:
	void process(Signal &signal);
};
class Straighten : public BasicBlock
{
public:
	void process(Signal &s);

};
class Shift : public BasicBlock
{
public:
	void process(Signal &signal);
};







void Amplify::process(Signal &s) {
	float c;
	cin >> c;
	for (int j = 0; j < 50; j++)
		s[j] = s[j] * c;
}
void Discretize::process(Signal &s) {
	float value_dis;
	cout << "Value of discretization:" << endl;
	cin >> value_dis;
	for (int j = 0; j<50; j++) {

		if (s[j] > value_dis) {
			s.setD(1, j);
		}
		else if (s[j] < value_dis) {
			s.setD(0, j);
		}

	}
}
void Intensify::process(Signal &signal) {
	float value;
	for (int j = 0; j < 50; j++) {
		value = signal[j] * signal[j];
		signal[j] = value;
	}
}
void Shift::process(Signal &signal) {
	float c;
	cin >> c;
	for (int j = 0; j < 50; j++)
		signal[j] = signal[j] + c;
}
float& Signal::operator[](int el) {
	return data[el];
}
int& Signal::operator()(int el) {
	return signals[el];
}
void Signal::readData(Signal &signal)
{
	ifstream file;
	file.open("Data.txt");
	file >> signal;
	file.close();
}
ifstream &operator >> (ifstream &stream, Signal &signal)
{
	for (int i = 0; i < 50; i++) {
		stream >> signal.x;
		signal.data.push_back(signal.x);
	}
	return stream;
}
void Signal::writeData(Signal &signal) {
	ofstream file;
	file.open("Result.txt");
	file << signal;
	file.close();

}
void Signal::setD(float Data, int iterator) {

	signals[iterator] = (int)Data;

}

ofstream &operator <<(ofstream &stream, Signal &signal) {
	for (int i = 0; i < 50; i++) {
		stream << "" << signal[i] << "\t"; 
		stream << "" << signal(i) << endl;
		
	}
	return stream;
}
void Straighten::process(Signal &s) {
	float c;
	for (int j = 0; j < 50; j++) {
		c = abs(s[j]);
		s[j] = c;
	}
}
int main()
{
	Signal signal;
	Intensify intensify;
	Amplify amplify;
	Shift shift;
	Straighten straighten;
	Discretize discretize;

	int choice;
	signal.readData(signal);

	do {

		cout << "How do you want to change your signal?" << endl; 
		cout << "Intensify - type 10 " << endl;
		cout << "Shift - type 20" << endl;
		cout << "Straighten - type 30" << endl;
		cout << "Amplify - type 40" << endl;
		cout << "Discretize - type 50" << endl;
		cout << "Save to file - type 60" << endl;
		cout << "Terminate - type 70" << endl;
		cout << "Give me a number " << endl;
		cin >> choice;
		switch (choice)
		{
		case 10:
			system("cls");
			intensify.process(signal);
			cout << "Completed intensify" << endl;
			break;

		case 20:
			system("cls");
			cout << " Value of amplify?: " << endl;
			amplify.process(signal);
			cout << "Completed amplify" << endl;
			break;

		case 30:
			system("cls");
			cout << "Value of shift" << endl;
			shift.process(signal);
			cout << "Completed shift" << endl;
			break;

		case 40:
			system("cls");
			straighten.process(signal);
			cout << "Completed straighten" << endl;
			break;

		case 50:
			system("cls");
			discretize.process(signal);
			cout << "Signal=" << signal[0] << endl;
			cout << "" << signal(0) << endl;
			cout << "Completed  discretize" << endl;
			break;

		case 60:
			system("cls");
			signal.writeData(signal);
			cout << "Zakonczono powodzeniem" << endl;
			break;

		}
	}
	while(choice != 70);
	
	
    return 0;
}