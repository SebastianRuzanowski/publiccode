#pragma once
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;
class Signal
{
private:
	vector<float> data;
	float x;
	int signals[50];
public:
	void readData(Signal &s);
	friend ifstream &operator>>(ifstream &str, Signal &signal);
	friend ofstream &operator <<(ofstream &str, Signal &signal);
	float& operator[](int el);
	void writeData(Signal &s);
	void setD(float Data, int iterator);
	int& operator()(int el);
};
