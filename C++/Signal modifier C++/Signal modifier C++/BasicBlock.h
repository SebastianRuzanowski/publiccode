#pragma once
#include "Signal.h"
class BasicBlock
{
public:
	virtual void process(Signal &stream) = 0;
};

