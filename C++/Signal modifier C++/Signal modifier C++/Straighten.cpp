#include "stdafx.h"
#include "Straighten.h"
#include <cmath>



void Straighten::process(Signal &s) {
	float c;
	for (int j = 0; j < 50; j++) {
		c = abs(s[j]);
		s[j] = c;
	}
}