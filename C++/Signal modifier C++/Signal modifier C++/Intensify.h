#pragma once
#include "Signal.h"
#include "BasicBlock.h"
class Intensify : public BasicBlock
{
public:
	void process(Signal &signal);
};

