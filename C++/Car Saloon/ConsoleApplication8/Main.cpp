﻿#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <string>
#include <fstream>
#include <Windows.h>
#include <list>

#define _CRT_SECURE_NO_WARNINGS
using namespace std;

class Pojazd 
{
protected:
	char nazwiskoPosiadacza[30];
	char numerRejestracji[30];
	char marka[30];
	int pojemnoscSilnika;
	int rokProdukcji;
};
class Samochod : public Pojazd
{
//	friend void Salon::DodajSamochodDoListy(Salon* salon);
	friend class Salon;
};
class Salon
{
private:
	list<Samochod> samochody;
	int iloscSamochodow;
	void RokProdukcji(Samochod* samochod);
	void PojemnoscSilnika(Samochod* samochod);
	friend class Samochod;
public:
	void DodajSamochodDoListy(Salon* samochod);
	void DodajSamochod();
	void PokazSalon();
	void WyparkujSamochod();
};

void Salon::RokProdukcji(Samochod* samochod)
{
	scanf("%d", &samochod->rokProdukcji);
		while (samochod->rokProdukcji >= 2017 || samochod->rokProdukcji <= 1899 || samochod->rokProdukcji == NULL)
		{
			if (samochod->rokProdukcji > 2017)
			{
				printf("Zla data. Dzisiaj mamy 2017 rok. Podano: %d\nSprobuj jeszcze raz:", samochod->rokProdukcji);
			}
			else if (samochod->rokProdukcji < 1899)
			{
				printf("Zla data. Nie bylo wtedy samochodow! Podano: %d\nSprobuj jeszcze raz: ", samochod->rokProdukcji);
			}
			else
				printf("Zla data. Sprobuj jeszcze raz:");
			scanf("%d", &samochod->rokProdukcji);
		}

}
void Salon::PojemnoscSilnika(Samochod* samochod)
{
	scanf("%d", &samochod->pojemnoscSilnika);
	while (samochod->pojemnoscSilnika<500 || samochod->pojemnoscSilnika>5000 || samochod->pojemnoscSilnika==NULL)
	{
		printf("Zla pojemnosc silnika. Pojemnosc silnika miesci sie miedzy 500cc, a 5000cc. Wpisana wartosc: %d \n Podaj pojemnosc silnika jeszcze raz:", samochod->pojemnoscSilnika);
		scanf("%d", &samochod->pojemnoscSilnika);
	}
}
void Salon::DodajSamochodDoListy(Salon* salon)
{

	Samochod* samochod = new Samochod();
	printf("podaj marke samochodu: \n");
	scanf("%s", samochod->marka);
	printf("podaj nazwisko\n");

	scanf("%s", samochod->nazwiskoPosiadacza);
	printf("podaj numer rejestracji samochodu\n");
	scanf("%s", samochod->numerRejestracji);

	printf("podaj pojemnosc silnika\n");
	PojemnoscSilnika(samochod);
	printf("podaj rok produkcji: ");
	RokProdukcji(samochod);

	salon->samochody.push_back(*samochod);
}
void Salon::DodajSamochod()
{
	Samochod* samochod = new Samochod();
	this->DodajSamochodDoListy(this); // Wywoluje fu
	samochod = &this->samochody.back();
	FILE *f = fopen("samochod.txt", "a+");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
	fprintf(f, "%s;%s;%s;%d;%d\n", samochod->marka, samochod->nazwiskoPosiadacza, samochod->numerRejestracji, samochod->pojemnoscSilnika, samochod->rokProdukcji);
	fclose(f);
	printf("\nPomyslnie zapisano.");
}
void Salon::PokazSalon()
{
	{
		system("cls");
		cout << "Twoje samochody to:" << endl;
		int c, enumerator = 0, i = 1, pierwszaLinia = 1, liczbaLinii = 0;
		FILE *file;
		file = fopen("samochod.txt", "r");
		if (file) {
			while ((c = getc(file)) != EOF)
			{
				if (c == '\n')liczbaLinii++;
			}
			fclose(file);
			file = fopen("samochod.txt", "r");
			while ((c = getc(file)) != EOF)
			{
				if (c == '\n' || pierwszaLinia == 1)
				{
					if (liczbaLinii + 1 == i)
						break;

					printf("\n");
					printf("%d. Marka: ", i);
					if (c != '\n')
					{

						putchar(c);
					}
					pierwszaLinia = 0;
					i++;
					enumerator++;
					continue;
				}

				if (c == ';')
				{
					switch (enumerator)
					{
					case 1:
						printf(", nazwisko wynajmujacego: ");
						break;
					case 2:
						printf(", numer rejestracyjny: ");
						break;
					case 3:
						printf(", pojemnosc silnika: ");
						break;
					case 4:
						printf(", rok produkcji: ");
						break;
					default:
						break;
					}
					enumerator++;
					if (enumerator >= 5)
					{
						enumerator = 0;
					}
				}
				else
					putchar(c);

			}
			fclose(file);
		}
		cout << endl << endl;
	}
}
void Salon::WyparkujSamochod()
{
	try {

		int liniaDoUsuniecia, aktualnaLinia = 1;
		string line;

		ifstream plik;
		plik.open("samochod.txt");
		ofstream podmiana;
		podmiana.open("tymczasowy.txt");
		cout << endl << "Ktory samochod chcesz wyparkowac? \nUWAGA: Przez wyparkowanie rozumie się usunięcie go z salonu na zawsze!";
		cin >> liniaDoUsuniecia;
		while (getline(plik, line))
		{
			if (aktualnaLinia != liniaDoUsuniecia)
			{
				podmiana << line << endl;
			}
			aktualnaLinia++;
		}
		podmiana.close();
		plik.close();
	
	
		if (plik.is_open())
			plik.close();

		if (remove("samochod.txt") != 0)
			perror("Blad usuwania samochod.txt");
		else
			puts("Samochod zostal usuniety pomyslnie");
		
		ifstream sprawdzenie("samochod.txt");
		if (sprawdzenie.good())
		{
			cout << "Niepoprawne zamkniecie pliku.";
		}

		rename("tymczasowy.txt", "samochod.txt");
		cout << endl << endl << endl;

	}
	catch (exception e)
	{
		cout << "\n**************\nCos poszlo nie tak w funkcji Wyparkuj Samochod.\n**************";
	}
}

int main()
{
	int wybor = 0;
	Salon* salon = new Salon();
	do
	{
		printf("\nSalon samochodowy\n");
		printf("1. Dodaj samochod.\n");
		printf("2. Sprawdz swoje samochody.\n");
		printf("3. Wyparkuj samochod z salonu.\n");
		printf("9. Wyjdz.\n\n");
		printf("Wybierz: ");
		scanf_s("%d", &wybor);
		if (wybor == 9)
			continue;

		switch (wybor)
		{
		case 1:
		salon->DodajSamochod();
			break;
		case 2:
			salon->PokazSalon();
			break;
		case 3:
			salon->WyparkujSamochod();
			break;
		default:
			printf("Wybrano niedobry przycisk!\n\n");
			main();
		}

	} while (wybor != 9);
	return 0;
}


