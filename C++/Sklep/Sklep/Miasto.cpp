﻿// Sklep.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <list>

using namespace std;

struct Wspolrzedne
{
	int x;
	int y;
};
enum Regiony
{
	dolnośląskie,
	kujawsko_pomorskie,
	lubelskie,
	lubuskie,
	włódzkie,
	małopolskie,
	mazowieckie,
	opolskie,
	podkarpackie,
	podlaskie,
	pomorskie,
	śląskie,
	świętokrzyskie,
	warmińsko_mazurskie,
	wielkopolskie,
	zachodniopomorskie,
};
class Miasto
{
public:
	Wspolrzedne *wspolrzedne = new Wspolrzedne();
	Regiony* region = new Regiony();
	string nazwa;
	signed int kodPocztowy;
	int iloscMieszkancow;
};
